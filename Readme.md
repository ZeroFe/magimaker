MagiMaker 종합 관리 저장소

Prefabs, Resources, Scenes, Script를 제외한 모든 파일은 외부 에셋으로 간주

Google Drive 등 에셋 저장소를 통해 받기 바람

따로 관리 목록에 올리고 싶은 항목이 있다면 .gitignore 추가 바람

(용량 제한이 있으므로 크기가 크지 않으면 추가바람) 





Prefabs : 방에 배치하는 몬스터, 지형지물 등 일회성으로 배치하는 Prefab

- 항목 별로 하위 폴더 생성 바람
  ex) Prefabs/ReadyRoom

Resources : Resource.Load()를 통해 불러들이는 Prefab 혹은 Sprite

- Prefab이라도 Resource.Load()를 통해 불러온다면 Resource에 넣음
  - Resource에 있는 Prefab의 부속 Prefab의 경우 어디에 넣을지 논의 필요
- Sprite는 되도록이면 Resource를 통해 불러오기보단 외부 에셋처럼 관리하기를 권장 
  (Resource에 들어가면 메모리 부하, 용량 증가)

Scenes : 게임에 필요한 씬 외 테스트용 씬, 씬 구성 Prefab, 환경값 등

- 테스트용 씬은 Scene/Test에 넣어서 관리 바람
- 씬 구성 Prefab은 UI 등 여러 씬에 다양하게 적용될 수 있는, 씬에 직접 넣어서 관리하는 프리팹만 넣길 바람
  ex) UI, MonsterManager
- 환경값은 Sky and Fog Settings Profile 등을 말하는 것임. 관련 값이 있다면 Scene에 같이 추가바람

