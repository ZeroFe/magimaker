﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ChangeConsumableEvent : UnityEvent<Sprite, string, int> { }

[System.Serializable]
public class CInventory
{
    // int에 들어가는 값은 Item Code
    public class AddItemEvent : UnityEvent<int> { }
    public AddItemEvent addItemEvent { get; set; } = new AddItemEvent();

    //int에 들어가는 값은 Earned Gold Amount
    public class AddGoldEvent : UnityEvent<int> { }
    public AddGoldEvent addGoldEvent { get; set; } = new AddGoldEvent();
    public class SubGoldEvent : UnityEvent<int> { }

    public SubGoldEvent subGoldEvent = new SubGoldEvent();


    public UnityEvent UseConsumableEvent { get; set; } = new UnityEvent();

    [System.Serializable]
    public class ConsumableWithStack
    {
        public Item.CConsumable consumable;
        public int stack;

        public ConsumableWithStack(Item.CConsumable _consumable, int _stack)
        {
            consumable = _consumable;
            stack = _stack;
        }
    }
    public List<Item.CEquip> EquipItems { get; private set; }
    public List<ConsumableWithStack> ConsumableItems { get; private set; }
    private int SelectedConsumableNumber
    {
        get
        {
            return _selectedConsumableNumber;
        }
        set
        {
            if (value == ConsumableItems.Count)
            {
                _selectedConsumableNumber = 0;
                if (_selectedConsumableNumber == ConsumableItems.Count)
                {
                    changeConsumableEvent.Invoke(
                        null,
                        "",
                        0);
                    return;
                }
            }
            else
            {
                _selectedConsumableNumber = value;
            }
            Debug.Log($"Current Consumable Number : {_selectedConsumableNumber}");

            changeConsumableEvent.Invoke(
                ConsumableItems[_selectedConsumableNumber].consumable.ItemImage,
                ConsumableItems[_selectedConsumableNumber].consumable.ItemName,
                ConsumableItems[_selectedConsumableNumber].stack);
        }
    }
    private int _selectedConsumableNumber;

    private GameObject _inventoryUser;

    public int Gold
    {
        get { return _gold; }
        set { _gold = value; }
    }
    private int _gold;

    private int[] _equipAbilityIncreaseSizeArr = new int[Enum.GetValues(typeof(Item.EEquipAbility)).Length];
    public int[] equipAbilityIncreaseSizeArrPublic = new int[Enum.GetValues(typeof(Item.EEquipAbility)).Length];

    public ChangeConsumableEvent changeConsumableEvent = new ChangeConsumableEvent();

    public CInventory(GameObject userObject, int equipCapacity = 10, int consumableCapacity = 3, int gold = 0)
    {
        _inventoryUser = userObject;
        EquipItems = new List<Item.CEquip>(equipCapacity);
        ConsumableItems = new List<ConsumableWithStack>(consumableCapacity);
        _selectedConsumableNumber = 0;

        _gold = gold;

        RegisterEquipEvent();
    }

    #region Implement Passive, Upgrade
    private void RegisterEquipEvent()
    {
        var inventoryUserPara = _inventoryUser.GetComponent<CPlayerPara>();
        var inventoryUserSkill = _inventoryUser.GetComponent<CPlayerSkill>();

        CPortalManager.Instance.EnterRoom.AddListener((int enteringRoomType) => CallItemEvent(Item.EEquipEvent.EnterRoom, 1));
        CPortalManager.Instance.EnterRoom.AddListener((int enteringRoomType) => CallItemEvent(Item.EEquipEvent.TypeEnterRoom, enteringRoomType));
        CPortalManager.Instance.EnterNextRoom.AddListener((int a, int b, int[] c) => CallItemEvent(Item.EEquipEvent.ClearRoom, 1));
        // 이벤트 진입, 이벤트 탈출 코드 넣어야 함
        // 몬스터 처치 코드 넣어야 함
        addGoldEvent.AddListener((int gold) => CallItemEvent(Item.EEquipEvent.AddGold, gold));
        addItemEvent.AddListener((int itemCode) => CallItemEvent(Item.EEquipEvent.AddItem, 1));
        addItemEvent.AddListener((int itemCode) => CallItemEvent(Item.EEquipEvent.TypeAddItem, Item.CItem.GetType(itemCode)));
        UseConsumableEvent.AddListener(() => CallItemEvent(Item.EEquipEvent.UseConsumable, 1));
        // 상점에서 구매한 액수, 횟수 코드 넣어야 함
        inventoryUserPara.DeadEvent.AddListener(() => CallItemEvent(Item.EEquipEvent.Death, 1));
        inventoryUserPara.TimeEvent.AddListener(() => CallItemEvent(Item.EEquipEvent.Time, 1));
        // 일정 이동거리마다 코드 넣어야 함
        if (_inventoryUser.GetComponent<CCntl>() != null)
        {
            var _inventoryUserFSM = _inventoryUser.GetComponent<CCntl>();
            _inventoryUserFSM.AttackEvent.AddListener(() => CallItemEvent(Item.EEquipEvent.Attack, 1));
            _inventoryUserFSM.RollEvent.AddListener(() => CallItemEvent(Item.EEquipEvent.Roll, 1));
        }
        else if (_inventoryUser.GetComponent<CMultiDoll>() != null)
        {
            var _inventoryUserFSM = _inventoryUser.GetComponent<CMultiDoll>();
            _inventoryUserFSM.AttackEvent.AddListener(() => CallItemEvent(Item.EEquipEvent.Attack, 1));
            _inventoryUserFSM.RollEvent.AddListener(() => CallItemEvent(Item.EEquipEvent.Roll, 1));
        }
        inventoryUserPara.TakeDamage.AddListener((int damageAmount) => CallItemEvent(Item.EEquipEvent.DamagedCount, 1));
        inventoryUserPara.TakeDamage.AddListener((int damageAmount) => CallItemEvent(Item.EEquipEvent.TakeDamage, damageAmount));
        inventoryUserPara.GiveDamage.AddListener((int damageAmount, GameObject giver) => CallItemEvent(Item.EEquipEvent.GiveDamage, damageAmount));
        inventoryUserPara.TakeHeal.AddListener((int healAmount) => CallItemEvent(Item.EEquipEvent.Heal, healAmount));
        inventoryUserSkill.skillUseEvent.AddListener((int skillNum, Vector3 pos) => CallItemEvent(Item.EEquipEvent.UseSkill, 1));
        inventoryUserSkill.skillUseEvent.AddListener((int skillNum, Vector3 pos) => CallItemEvent(Item.EEquipEvent.TypeUseSkill, skillNum));
    }

    /// <summary>
    /// 해당 이벤트가 일어나면 장비 효과 발동(패시브, 성장)
    /// </summary>
    /// <param name="condition">패시브 발동 조건</param>
    /// <param name="count">패시브 조건 인자</param>
    private void CallItemEvent(Item.EEquipEvent condition, int count)
    {
        foreach (var equip in EquipItems)
        {
            if (equip.PassiveCondition == condition)
            {
                ExecuteEquipPassive(equip, count);
            }

            if (equip.UpgradeCondition == condition)
            {
                ExecuteEquipUpgrade(equip);
            }
        }
    }

    /// <summary>
    /// 장비 패시브를 추가 조건에 따라 실행
    /// ex) n회에 한 번 실행 / n 이상일 때 실행 / n 이하일 때 실행
    /// </summary>
    /// <param name="equip">적용 장비</param>
    /// <param name="count"></param>
    private void ExecuteEquipPassive(Item.CEquip equip, int count)
    {
        switch (equip.PassiveConditionOption)
        {
            case Item.EEquipEventCountOption.Accumulate:
                equip.passiveCurrentCount += count;
                if (equip.passiveCurrentCount >= equip.PassiveUseCount)
                {
                    _inventoryUser.GetComponent<CPlayerPara>().TakeUseEffectHandleList(equip.passiveEffect, _inventoryUser);
                    equip.passiveCurrentCount = 0;
                }
                break;
            case Item.EEquipEventCountOption.Each_Below:
                if (equip.PassiveUseCount >= count)
                {
                    _inventoryUser.GetComponent<CPlayerPara>().TakeUseEffectHandleList(equip.passiveEffect, _inventoryUser);
                }
                break;
            case Item.EEquipEventCountOption.Each_Over:
                if (equip.PassiveUseCount <= count)
                {
                    _inventoryUser.GetComponent<CPlayerPara>().TakeUseEffectHandleList(equip.passiveEffect, _inventoryUser);
                }
                break;
            case Item.EEquipEventCountOption.EqualType:
                if (equip.PassiveUseCount == count)
                {
                    _inventoryUser.GetComponent<CPlayerPara>().TakeUseEffectHandleList(equip.passiveEffect, _inventoryUser);
                }
                break;
            default:
                Debug.Log("Error case");
                break;
        }
    }

    /// <summary>
    /// 장비 성장
    /// </summary>
    /// <param name="equip"></param>
    private void ExecuteEquipUpgrade(Item.CEquip equip)
    {
        if (equip.UpgradeCurrentCount >= equip.UpgradeCount)
        {
            return;
        }

        equip.UpgradeCurrentCount++;
        if (equip.UpgradeCurrentCount == equip.UpgradeCount)
        {
            Debug.Log("Equip Upgrade");
        }
    }
    #endregion

    #region Add / Delete Item
    public bool AddItem(Item.CItem newItem)
    {
        if (newItem is Item.CEquip)
        {
            return AddEquip(newItem as Item.CEquip);
        }
        else if (newItem is Item.CConsumable)
        {
            return AddConsumableItem(newItem as Item.CConsumable);
        }
        else
        {
            Debug.Log("Can't Add Unknown Type Item");
            return false;
        }
    }

    /// <summary>
    /// 가방에서 아이템 빼기
    /// </summary>
    /// <returns></returns>
    public bool DeleteEquipItem(int itemIndex)
    {
        if (itemIndex < 0 || EquipItems.Count <= itemIndex)
        {
            Debug.Log("Can't Delete Equip Item");
            return false;
        }

        // 귀속 아이템 여부 판단

        // 버리기 처리
        var itemObject = CItemManager.instance.GetItemObject(EquipItems[itemIndex].ItemCode);
        itemObject.SetActive(true);
        itemObject.transform.position = _inventoryUser.transform.position;
        DeleteEquipAbility(EquipItems[itemIndex]);
        EquipItems.RemoveAt(itemIndex);

        return true;
    }

    /// <summary>
    /// 가방에 장비 아이템 넣기
    /// </summary>
    /// <returns>아이템 넣기 성공했는지</returns>
    private bool AddEquip(Item.CEquip newEquip)
    {
        if (EquipItems.Count >= EquipItems.Capacity)
        {
            return false;
        }

        if (HasOverlapEquip(newEquip))
        {
            Debug.Log("overlap equip");
            return false;
        }

        EquipItems.Add(newEquip);
        AddEquipAbility(newEquip);

        addItemEvent.Invoke(newEquip.ItemCode);

        return true;
    }

    /// <summary>
    /// 가방에 소비 아이템 넣기
    /// </summary>
    /// <returns>아이템 넣기 성공했는지</returns>
    private bool AddConsumableItem(Item.CConsumable newItem)
    {
        if (ConsumableItems.Count >= ConsumableItems.Capacity)
        {
            return false;
        }

        int overlapIndex;
        if ((overlapIndex = HasOverlapConsumable(newItem)) != -1)
        {
            ConsumableItems[overlapIndex].stack++;
        }
        else
        {
            ConsumableItems.Add(new ConsumableWithStack(newItem, 1));
        }

        // 이벤트 발생용
        SelectedConsumableNumber = SelectedConsumableNumber;

        return true;
    }
    #endregion

    #region Earn / Lost Gold
    /// <summary>
    /// amount만큼 골드를 획득하고 isInvokeEvent에 따라 골드 획득 이벤트를 호출한다
    /// </summary>
    /// <param name="amount">획득량</param>
    /// <param name="isInvokeEvent">골드 획득 이벤트 발생 여부</param>
    public void AddGold(int amount, bool isInvokeEvent)
    {
        Debug.Log("before Gold = " + _gold);

        _gold += amount;

        if (isInvokeEvent)
            addGoldEvent?.Invoke(amount);

        Debug.Log("after Gold = " + _gold);
    }

    public void SubGold(int gold, bool isSubGoldEvent)
    {
        Debug.Log("before Gold = " + _gold);

        if (isSubGoldEvent)
        {
            subGoldEvent?.Invoke(gold);
            return;
        }

        _gold -= gold;


        Debug.Log("after Gold = " + _gold);
    }
    #endregion

    public void GetNextConsumable()
    {
        if (ConsumableItems.Count == 0)
        {
            Debug.Log("Consumable inventory is empty");
            return;
        }

        ++SelectedConsumableNumber;
    }

    public void UseSelectedConsumable()
    {
        if (ConsumableItems.Count == 0)
        {
            Debug.Log("consumable empty");
            return;
        }

        Debug.Log($"Selected Consumable Number : {SelectedConsumableNumber}");
        UseConsumable(ConsumableItems[SelectedConsumableNumber].consumable);
        if (--ConsumableItems[SelectedConsumableNumber].stack <= 0)
        {
            ConsumableItems.RemoveAt(SelectedConsumableNumber);
            SelectedConsumableNumber = 0;
        }
    }

    private void SwapEquip(ref Item.CEquip lhs, ref Item.CEquip rhs)
    {
        Item.CEquip temp;
        temp = lhs;
        lhs = rhs;
        rhs = temp;
    }

    #region EquipAbility
    public int GetEquipAbilityIncreaseSize(Item.EEquipAbility ability)
    {
        return _equipAbilityIncreaseSizeArr[(int)ability];
    }

    /// <summary>
    /// 장비 아이템의 합산 능력치 및 효과 갱신
    /// </summary>
    private void AddEquipAbility(Item.CEquip equip)
    {
        foreach (var ability in equip.equipAbilities)
        {
            _equipAbilityIncreaseSizeArr[(int)ability.equipEffect] += ability.value;
        }
    }

    public void AddAbility(int value, Item.EEquipAbility type)
    {
        equipAbilityIncreaseSizeArrPublic[(int)type] += value;
    }

    private void DeleteEquipAbility(Item.CEquip equip)
    {
        foreach (var ability in equip.equipAbilities)
        {
            _equipAbilityIncreaseSizeArr[(int)ability.equipEffect] -= ability.value;
        }
    }
    #endregion

    ///<summary>
    ///장비 상위 / 하위 아이템 중복인지 확인
    ///</summary>
    ///<returns></returns>
    private bool HasOverlapEquip(Item.CEquip newEquip)
    {
        return false;
    }

    /// <summary>
    /// 소비 아이템 중복인지 확인 및 중복인 소비 아이템 인덱스 리턴
    /// </summary>
    /// <param name="newConsumable"></param>
    /// <returns></returns>
    private int HasOverlapConsumable(Item.CConsumable newConsumable)
    {
        for (int index = 0; index < ConsumableItems.Count; index++)
        {
            if (ConsumableItems[index].consumable.ItemCode == newConsumable.ItemCode)
            {
                return index;
            }
        }
        return -1;
    }

    private void UseConsumable(Item.CConsumable consumable)
    {
        _inventoryUser.GetComponent<CPlayerPara>().TakeUseEffectHandleList(consumable.UseEffectList, _inventoryUser);
        UseConsumableEvent?.Invoke();
    }
}
