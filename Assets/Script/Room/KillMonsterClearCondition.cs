﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillMonsterClearCondition : MonoBehaviour
{
    public GameObject targetMonster = null;

    // Start is called before the first frame update
    void Start()
    {
        if (targetMonster)
        {
            Debug.Log($"Kill Monster!");
            targetMonster.GetComponent<CEnemyPara>().DeadEvent.AddListener(ClearRoom);
        }
        else
        {

        }
    }

    private void ClearRoom()
    {
        CGlobal.isClear = true;
        CCreateMap.instance.NotifyPortal();
    }
}
