﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class CEventRoom8Manager : MonoBehaviour
{
    [SerializeField]
    public VolumeProfile profile;
    private void Start()
    {
        GameObject.Find("Directional Light").SetActive(false);
        CSkyFogController.instance.ChangeVolume(profile);

        CGlobal.isEvent = true;
        CCreateMap.instance.NotifyPortal();
    }
}
