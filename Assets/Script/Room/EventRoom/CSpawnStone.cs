﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

//해야할 것 : 돌 소환 직전에 마법진 띄워주기, 돌 바닥에서부터 올라오는 연출, 포탈방
public class CSpawnStone : MonoBehaviour
{
    [System.Serializable]
    private class SpawnInfo
    {
        public int spawnCount;
        public float percentChance;
    }

    [SerializeField] private GameObject _stonePrefab;
    GameObject _stoneSpawner;
    [Header("돌 생성 개수 설정")]
    [SerializeField] private List<SpawnInfo> _spawnInfos = new List<SpawnInfo>();
    private List<int>[] _spawnCombinationsArr = null;

    [Header("돌 생성 주기 설정")]
    [SerializeField] private float _initWaitingTime = 5.0f;
    [SerializeField] private float _minWaitingTime = 2.0f;
    [SerializeField] private float _maxWaitingTime = 4.0f;
    private float _waitingTime = 0.0f;
    float _timer = 0.0f;
    private int _stoneSpawnerCount = 0;

    void Start()
    {
        _stoneSpawner = gameObject;
        _waitingTime = _initWaitingTime;
        _stoneSpawnerCount = transform.childCount;
        _spawnCombinationsArr = new List<int>[_spawnInfos.Count];
        for (int i = 0; i < _spawnInfos.Count; i++)
        {
            _spawnCombinationsArr[i] = new List<int>();
        }
        // 돌 조합 미리 설정하기
        for (int seed = 0; seed < (1 << _stoneSpawnerCount); seed++)
        {
            var oneBitsCount = GetOneBitsCount(seed, _stoneSpawnerCount);
            for (int idx = 0; idx < _spawnInfos.Count; idx++)
            {
                if (_spawnInfos[idx].spawnCount == oneBitsCount)
                {
                    _spawnCombinationsArr[idx].Add(seed);
                    break;
                }
            }
        }

        for (int i = 0; i < transform.childCount; i++)
            _stoneSpawner.transform.GetChild(i).gameObject.SetActive(false);
    }

    void Update()
    {
        _timer += Time.deltaTime;

        if (_timer > _waitingTime) //이 코드를 통해서 돌이 너무 빠르게 나오지 않게함.
        {
            SpawnStone();

            _timer = 0;
            _waitingTime = Random.Range(_minWaitingTime, _maxWaitingTime);
        }
    }

    private void OnDestroy()
    {
        Debug.Log("Destroy Stone");

        GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("EventRoomStone");

        foreach (GameObject gameObject in gameObjects)
            Object.Destroy(gameObject);
    }

    private void SpawnStone()
    {
        var selectedIndex = SelectSpawnInfoIndex();
        if (selectedIndex == -1)
        {
            return;
        }
        var selectedCombinationIndex = Random.Range(0, _spawnCombinationsArr[selectedIndex].Count);
        int stoneSpawnSeed = _spawnCombinationsArr[selectedIndex][selectedCombinationIndex];
        
        int pos = 0;
        while (stoneSpawnSeed > 0)
        {
            if ((stoneSpawnSeed & 1) == 1)
            {
                StartCoroutine(OnMagicCircle(pos));
            }
            stoneSpawnSeed >>= 1;
            pos += 1;
        }
    }

    IEnumerator OnMagicCircle(int pos)  //돌이 생성되기 1초전 마법진 빛나줌.
    {
        _stoneSpawner.transform.GetChild(pos).gameObject.SetActive(true);
        yield return new WaitForSeconds(1);
        _stoneSpawner.transform.GetChild(pos).gameObject.SetActive(false);
        Transform stoneSpawnerChild = _stoneSpawner.transform.GetChild(pos);
        Instantiate(_stonePrefab, stoneSpawnerChild.position, Quaternion.identity);
    }

    /// <summary>
    /// 0번 비트부터 inspectBitCount-1번 비트까지 1의 개수를 구한다
    /// </summary>
    /// <param name="num">1의 개수를 구할 숫자</param>
    /// <param name="inspectBitCount">몇 번째 비트까지 검사할지</param>
    /// <returns>1의 개수</returns>
    private static int GetOneBitsCount(int num, int inspectBitCount)
    {
        int count = 0;
        for (int i = 0; i < inspectBitCount; i++)
        {
            if ((num & 1) == 1)
            {
                count++;
            }
            num >>= 1;
        }
        return count;
    }

    private int SelectSpawnInfoIndex()
    {
        if (_spawnInfos.Count == 0)
        {
            Debug.LogWarning("list count is 0. can't select index");
            return -1;
        }
        else if (_spawnInfos.Count == 1)
        {
            return 0;
        }

        float chanceSum = 0;
        List<float> chanceSumList = new List<float>(_spawnInfos.Count);
        foreach (var effect in _spawnInfos)
        {
            chanceSum += effect.percentChance;
            chanceSumList.Add(chanceSum);
        }

        // 임의 효과 선택
        int randomChance = UnityEngine.Random.Range(0, 100);
        int idx = 0;
        while (idx < chanceSumList.Count - 1 && randomChance >= chanceSumList[idx])
        {
            idx++;
        }

        return idx;
    }
}