﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CTimeStopRunner : CRunner
{
    public static float skillTime = 2f;
    public GameObject test1;
    public GameObject test2;
    public GameObject test3;
    public override void Skill(CEventRoom7Manager.RunnerInfo runnerInfo)
    {
        test1.GetComponent<CRunner>().isTimeStop = true;
        test2.GetComponent<CRunner>().isTimeStop = true;
        test3.GetComponent<CRunner>().isTimeStop = true;
    }
}
