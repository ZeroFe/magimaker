﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBlinkRunner : CRunner
{
    int blinkDistance = 5;
    public override void Skill(CEventRoom7Manager.RunnerInfo runnerInfo)
    {
        transform.position += new Vector3(0, 0, blinkDistance);
    }
}
