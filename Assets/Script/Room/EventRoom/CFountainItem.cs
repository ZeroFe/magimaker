﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CFountainItem : CFountainItemTrigger
{
    private GameObject _item;
    public override void Start()
    {
        base.Start();
        _item = CItemManager.instance.DropRandomItem(CCreateMap.instance.StageNumber, CConstants.EQUIP_ITEM_TYPE);
    }
    public override void GetReward()
    {
        if(!MoveItemToInventory(_item.GetComponent<CItemComponent>()))
            Debug.LogWarning("MoveItemFail");

        Object.Destroy(gameObject);
    }

    private bool MoveItemToInventory(CItemComponent itemComponent)
    {
        bool check = false;

        if (itemComponent != null)
        {
            check = _playerPara.Inventory.AddItem(itemComponent.Item);
        }

        return check;
    }
}
