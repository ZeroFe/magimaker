﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class MoveCursorEvent : UnityEvent<int> { }
public class CNPCPopUpController : MonoBehaviour
{
    public static CNPCPopUpController Instance = null;

    protected MoveCursorEvent _moveCursorEvent = new MoveCursorEvent(); //커서 움직일 경우 이벤트
    protected GameObject _popUp;
    private int _childCount;
    private int _choose; //0이면 첫번째 선택 1이면 2번째 선택
    protected GameObject _NPC;
    [HideInInspector]
    public int optionChoosed;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    // Start is called before the first frame update
    public virtual void Start()
    {
        _popUp = gameObject;
        _NPC = GameObject.FindGameObjectWithTag("NPC");

        _childCount = _popUp.transform.childCount;
        for (int i = 0; i < _childCount; i++) //버튼이 아닌 경우 제외 위함
        {
            if (_popUp.transform.GetChild(i).tag == "NoButton")
                _childCount--;
        }

        _popUp.transform.GetChild(0).GetComponent<Image>().color = Color.white; //1번째 선택 처음에 되있음. 하이라이트 = 흰색
        _choose = 0;

        for (int i = 1; i < _childCount; i++)
            if (_popUp.transform.GetChild(i).tag != "NoButton")
                _popUp.transform.GetChild(i).GetComponent<Image>().color = Color.grey;

        Debug.Log("NPC Popup");

        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow)) //위 방향키
        {
            if (_choose > 0)
                _choose--;
        }

        if (Input.GetKeyDown(KeyCode.DownArrow)) //아래 방향키
        {
            if (_choose < (_childCount - 1))
                _choose++;
        }

        highlightSet(_choose);

        if (Input.GetKeyDown(KeyCode.Return)) //엔터 입력 시
        {
            ChooseButton(_choose);

            if (CGlobal.popUpCancel)
            {
                //팝업 꺼지므로 플레이어 이동 안막힘
                CGlobal.popUpCancel = false;
                CEventRoomNpcClick.instance.CancelPopUp();
            }
        }
    }

    public virtual void ChooseButton(int choose) { }

    void highlightSet(int choose)
    {
        for (int i = 0; i < _childCount; i++)
            if (_popUp.transform.GetChild(i).tag != "NoButton") //인포창까지 회색되는거 방지
                _popUp.transform.GetChild(i).GetComponent<Image>().color = Color.grey;

        _popUp.transform.GetChild(choose).GetComponent<Image>().color = Color.white;
        _moveCursorEvent.Invoke(choose);
    }

    protected void SendPopUpChooseNumber(int choose) //서버로 선택한 내용 보냄
    {
        if (Network.CTcpClient.instance != null)
            Network.CNetworkEvent.SendPopUpChooseNumber(choose);
    }

    virtual public void ReceiveChoosePlayerNumber(int choose) //서버에서 다른 사람들이 선택한 내용 받음
    {
    }
}
