﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CEventRoom8PopUpController : CNPCPopUpController
{
    // Start is called before the first frame update
    override public void Start()
    {
        base.Start();
    }

    public override void ChooseButton(int choose)
    {
        switch(choose)
        {
            case 0:
                ActivePortal();
                break;
            case 1:
                break;
        }

        CGlobal.popUpCancel = true;
    }

    private void ActivePortal()
    {
        GameObject.Find("Directional Light").SetActive(true);
        CSkyFogController.instance.ChangeToOriginal();
        CGlobal.isEvent = false;
        CCreateMap.instance.NotifyPortal();
    }

}
