﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CEventRoom8Ring : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        int[] random = new int[transform.childCount];
        int doorCount = 1;
        int temp, dst, src;

        for(int i = 0; i < random.Length; i++)
        {
            random[i] = i;
        }

        for(int i = 0; i < 100; i++)
        {
            dst = Random.Range(0, random.Length);
            src = Random.Range(0, random.Length);

            temp = random[dst];
            random[dst] = random[src];
            random[src] = temp;
        }

        for(int i = 0; i < doorCount; i++)
        {
            transform.GetChild(random[i]).gameObject.SetActive(false);
        }
    }
}
