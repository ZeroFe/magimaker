﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAccelRunner : CRunner
{
    float accelSpeed = 14f;
    public override void Skill(CEventRoom7Manager.RunnerInfo runnerInfo)
    {
        runnerInfo.speed = accelSpeed;
    }
}
