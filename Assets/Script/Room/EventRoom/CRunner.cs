﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CRunner : MonoBehaviour
{
    float originalSpeed;
    public int playerNum;

    private Animator animator;

    private readonly int hashRun = Animator.StringToHash("isRun");
    [HideInInspector]
    public bool isTimeStop = false;

    private int debug;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }
    IEnumerator Run(CEventRoom7Manager.RunnerInfo runnerInfo)
    {
        originalSpeed = runnerInfo.speed;
        float time = 0f;
        for (int i = 0; i < runnerInfo.repeatTime; i++)
        {
            debug = i;
            if (runnerInfo.skillTable[i])
            {
                Skill(runnerInfo);
            }
            else
            {
                while (time < 1f)
                {
                    animator.SetBool(hashRun, true);
                    if (isTimeStop) //시간 정지 발동
                    {
                        Debug.Log("isTimeStop");
                        break;
                    }
                    time += Time.deltaTime;
                    transform.position += new Vector3(0, 0, runnerInfo.speed * 0.01f);
                    yield return null;
                }
                time = 0f;

                if (isTimeStop) //시간 정지
                {
                    animator.SetBool(hashRun, !isTimeStop);
                    while (time < CTimeStopRunner.skillTime)
                    {
                        time += Time.deltaTime;
                        yield return null;
                    }
                    time = 0f;
                    isTimeStop = false;
                }

                runnerInfo.speed = originalSpeed;
            }
        }

        animator.SetBool(hashRun, false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "WALL")
        {
            Debug.Log(gameObject + " : " + debug);
            if (CGlobal.isEvRoom7Victory)
            {
                Debug.Log(gameObject + " is Victory!!!");
                CGlobal.isEvRoom7Victory = false;
                CEventRoom7Manager.instance.RewardVictory(playerNum);
                CEventRoom7Manager.instance.StartCoruSetVictoryText_PortalSet(playerNum); //승자가 누군지 보상은 얼마인지 알려줌
            }
        }
    }

    virtual public void Skill(CEventRoom7Manager.RunnerInfo runnerInfo) { }
}
