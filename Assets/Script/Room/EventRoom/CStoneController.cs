﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CStoneController : MonoBehaviour
{
    [Tooltip("돌 맞았을 때 효과")]
    public List<CUseEffectHandle> useEffects;

    [SerializeField] private float _speed = 10;
    private bool _upFinish = false; //돌 올라갔는지 확인할 플래그
    private int _recentCollisionInstanceID = 0;
    void Start()
    {
        Debug.Log("I'm Spawned");
        _speed = 10;
        _upFinish = false;
    }

    void Update()
    {
        if (_upFinish) //돌이 다 올라왔음
        {
            gameObject.transform.Rotate(new Vector3(-100, 0, 0) * Time.deltaTime * _speed);
            transform.position = Vector3.MoveTowards(transform.position, transform.position + new Vector3(0, 0, -100), _speed * Time.deltaTime);
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, transform.position + new Vector3(0, 100, 0), _speed * Time.deltaTime);
        }

        if (transform.position.y >= transform.localScale.y / 2 - 0.2) //돌의 높이
            _upFinish = true;

    }

    private void OnDisable()
    {
        Debug.Log($"Disabled Time : {Time.realtimeSinceStartup}");
    }

    private void OnDestroy()
    {
        Debug.Log($"Destroyed Time : {Time.realtimeSinceStartup}");
    }

    #region Hit Implementation
    private void OnTriggerEnter(Collider other)
    {
        if (!IsTriggeredRecently(other))
        {
            GetUseEffect(other);
        }

        if (other.gameObject.tag == "WALL" || other.gameObject.tag == "Player" || other.gameObject.tag == "Allies")
        {
            Debug.Log($"Entered {Time.realtimeSinceStartup}");
            Destroy(gameObject);
            //gameObject.SetActive(false);
        }
    }

    protected bool IsTriggeredRecently(Collider other)
    {
        int recentCollisionID = other.gameObject.GetInstanceID();
        if (_recentCollisionInstanceID == recentCollisionID)
        {
            return true;
        }
        return false;
    }

    protected void GetUseEffect(Collider other)
    {
        var cPara = other.GetComponent<CharacterPara>();
        if (cPara != null)
        {
            cPara.TakeUseEffectHandleList(useEffects, null);
        }
    }
    #endregion
}