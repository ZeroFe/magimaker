﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CEventRoom7Manager : MonoBehaviour
{
    public GameObject[] runner = new GameObject[4];
    public float[] runnerSpeed = new float[4];
    public GameObject victoryText;

    [HideInInspector]
    static public int victoryNum;
    public int selectRunner;

    RunnerInfo[] runnerInfo = new RunnerInfo[4];

    private string[] runnerName = { "사슴", "늑대", "토끼", "염소" };

    public static CEventRoom7Manager instance = null;
    public class RunnerInfo
    {
        public int repeatTime { get; private set; }
        public float speed;
        public bool[] skillTable;

        public RunnerInfo(float speed, int skillCount)
        {
            repeatTime = 20;
            this.speed = speed;
            skillTable = new bool[repeatTime];

            for (int i = 0; i < skillTable.Length; i++)
            {
                skillTable[i] = false;
            }

            while (skillCount > 0)
            {
                if (!skillTable[Random.Range(0, repeatTime * 3 / 4)])
                {
                    skillTable[Random.Range(0, repeatTime * 3 / 4)] = true;
                    skillCount--;
                }
            }
        }
    }

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        InitRunnerInfo();

        CGlobal.isEvent = true; // 경주 중에 포탈 사용해서 나가버리는 것 방지
        // 지역 변수로 고칠 것
        CGlobal.isEvRoom7Victory = true;
        CCreateMap.instance.NotifyPortal();
    }

    private void SendRunnerInfoToServer()
    {
        bool[,] skillTable = new bool[4, 20];

        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 20; j++)
            {
                skillTable[i, j] = runnerInfo[i].skillTable[j];
            }
        }

        Network.CNetworkEvent.SendEventRoom7SkillTable(skillTable);
    }

    public void ReceiveRunnerInfoToServer(bool[,] skillTable)
    {
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 20; j++)
            {
                runnerInfo[i].skillTable[j] = skillTable[i, j];
            }
        }


    }

    public void RewardVictory(int victoryNum)
    {
        Debug.Log("victory num : " + victoryNum + " selectRunner : " + selectRunner);
        if (victoryNum == selectRunner)
        {
            (CRaceNpcPopUpController.Instance as CRaceNpcPopUpController).RewardGold(victoryNum);
        }
    }

    public void StartCoruSetVictoryText_PortalSet(int victoryNum) //경기가 끝났음. 포탈 복구와 승리문구 추가. 카메라도 복구
    {
        CGlobal.isEvent = false;//포탈 복구
        CCreateMap.instance.NotifyPortal();
        CMouseFollower.instance.SetOriginalTarget();

        StartCoroutine(SetVictoryText(victoryNum)); //승리 문구 추가
    }

    IEnumerator SetVictoryText(int victoryNum) //승자, 보상 알려주는 텍스트 띄워주기
    {
        victoryText.SetActive(true);
        victoryText.transform.GetChild(0).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text =
            runnerName[victoryNum] + " 선수 승리!!!\n배당금 " +
            (CRaceNpcPopUpController.Instance as CRaceNpcPopUpController).CalcBetGold(victoryNum) + "골드";

        yield return new WaitForSeconds(5f); //5초간 띄워주고 끄기
        victoryText.SetActive(false);
    }

    private void InitRunnerInfo()
    {
        runnerInfo[0] = new RunnerInfo(runnerSpeed[0], 0);
        for (int i = 1; i < 4; i++)
            runnerInfo[i] = new RunnerInfo(runnerSpeed[i], Random.Range(2, 3));

        if (CClientInfo.JoinRoom.IsHost && Network.CTcpClient.instance && Network.CTcpClient.instance != null)
            SendRunnerInfoToServer();
    }

    public void StartRace()
    {
        CMouseFollower.instance.SetNewTarget(runner[selectRunner].transform);

        for(int i = 0; i < 4; i++)
            runner[i].SendMessage("Run", runnerInfo[i]);
    }
}
