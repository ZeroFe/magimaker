﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CRaceNpcPopUpController : CNPCPopUpController
{
    public GameObject manager;
    public GameObject infoTextObj;

    private string[] _playerText = {
        "능력 : 없음\n속도 : 가장 빠름\n베팅 가능 금액 : 100골드, 골드가 없으면 HP 10\n현재 배당금 : ",
        "능력 : 가속 - 일정시간 속도 증가\n속도 : 빠름\n베팅 가능 금액 : 100골드, 골드가 없으면 HP 10\n현재 배당금 : ",
        "능력 : 점멸 - 일정거리 순간이동\n속도 : 보통\n베팅 가능 금액 : 100골드, 골드가 없으면 HP 10\n현재 배당금 : ",
        "능력 : 시간정지 - 일정시간 다른 선수들 이동 멈춤\n속도 : 느림\n베팅 가능 금액 : 100골드, 골드가 없으면 HP 10\n현재 배당금 : " };
    private int[] _betGold = new int[4];
    private bool isFirst = true;
    private bool isRace = false;

    // Start is called before the first frame update
    override public void Start()
    {
        _moveCursorEvent.AddListener((int choose) => RefreshInfo(choose));
        InitBetGold();

        base.Start();
    }

    private void RefreshInfo(int choose) //선수 정보창 변경하기
    {
        if (choose < 4) //선수 숫자 4명
        {
            Instance.optionChoosed = choose;
            infoTextObj.transform.parent.gameObject.SetActive(true);
            infoTextObj.GetComponent<TMPro.TextMeshProUGUI>().text = _playerText[choose] + CalcBetGold(choose) + "골드";
        }
        else //취소버튼이나 테스트 버튼으로 커서 옮기는 경우
            infoTextObj.transform.parent.gameObject.SetActive(false);
    }

    public int CalcBetGold(int choose)
    {
        int betGold;
        return betGold = (_betGold[0] + _betGold[1] + _betGold[2] + _betGold[3]) / (_betGold[choose] / 100);
    }

    private void InitBetGold() //베팅 초기 금액 설정
    {
        for (int i = 0; i < 4; i++)
        {
            _betGold[i] = Random.Range(i + 1, i + 3) * 100;
        }
    }

    public override void ChooseButton(int choose)
    {
        switch (choose)
        {
            case 0:
            case 1:
            case 2:
            case 3:
                ChoosePlayer(choose);
                break;
            case 4:
                ClickCancel();
                break;
            case 5:
                SendStartMessageToServer(); //레이스 시작
                break;
        }

        CGlobal.popUpCancel = true;
    }

    private void ChoosePlayer(int choose)
    {
        if (isRace) //경기 시작 했으면 베팅 안됨
            return;

        if (isFirst)
        {
            UseGoldOrHP();
            AddBetGold(choose);
            SendPopUpChooseNumber(choose);
            CEventRoom7Manager.instance.selectRunner = choose;
            isFirst = false;
        }
    }

    private void UseGoldOrHP() //베팅할 돈을 건다. 돈이 없다면 돈대신 hp소모
    {
        Debug.Log("UseGoldOrHP");

        CPlayerPara player = CController.instance.player.GetComponent<CPlayerPara>();
        if (player.Inventory.Gold >= 100) //골드가 100 이상이면 골드 소모
            player.Inventory.SubGold(100, false);
        else //없으면 체력 소모
            player._curHp -= 10;
    }

    override public void ReceiveChoosePlayerNumber(int choose) //서버에서 다른 사람들이 선택한 내용 받음
    {
        AddBetGold(choose);
    }

    private void AddBetGold(int choose) //고른 사람들 돈 추가하고 정보 리프레쉬
    {
        _betGold[choose] += 100;
        RefreshInfo(choose);
    }

    public void RewardGold(int victoryNum) //골드 보상 해주기
    {
        CPlayerPara player = CController.instance.player.GetComponent<CPlayerPara>();
        player.Inventory.AddGold(CalcBetGold(victoryNum), false);
    }

    public void ClickCancel() { }

    public void SendStartMessageToServer() //레이스 시작명령 서버로 보냄
    {
        if(Network.CTcpClient.instance != null)
            Network.CNetworkEvent.SendEventRoom7StartRace(true);
        StartRace(true);
    }

    private void StartRace(bool start)
    {
        if (start && !isRace)
        {
            isRace = true;
            CGlobal.isEvent = true;
            CCreateMap.instance.NotifyPortal();
            manager.SendMessage("StartRace");
        }
    }

    public void ReceiveStartRace(bool start) //네트워크로부터 출발 명령 받음
    {
        if (start && !isRace)
        {
            isRace = true;
            CGlobal.isEvent = true;
            CCreateMap.instance.NotifyPortal();
            manager.SendMessage("StartRace");
        }
    }
}
