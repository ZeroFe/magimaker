﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CEventRoom0_5PortalElite : CEventRoom0_5Portal
{
    protected override void RewardSet()
    {
        StartCoroutine(RewardSetActiveTrue());
    }

    IEnumerator RewardSetActiveTrue()
    {
        yield return new WaitForSeconds(1.0f);

        GameObject.Find("EventRoom0_5Reward").transform.GetChild(1).gameObject.SetActive(true);
        GameObject.Find("EventRoom0_5Reward").transform.GetChild(0).gameObject.SetActive(true);
    }
}
