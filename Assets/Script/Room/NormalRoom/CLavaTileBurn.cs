﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CLavaTileBurn : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag == "Player")
        {
            Debug.Log("화상 걸기");
            transform.GetChild(0).GetComponent<CUseEffect>().TakeUseEffect(collision.transform.GetComponent<CPlayerPara>(), null);
        }

    }
}
