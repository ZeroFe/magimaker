﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CSwampTileWet : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag == "Player")
        {
            transform.GetChild(0).GetComponent<CUseEffect>().TakeUseEffect(collision.transform.GetComponent<CPlayerPara>(), null);
            //첨습 상태이상 걸기
            Debug.Log("첨습 걸기");
        }
    }
}
