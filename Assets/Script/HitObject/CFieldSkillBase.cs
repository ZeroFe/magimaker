﻿using UnityEngine;

public class CFieldSkillBase : CHitObjectBase
{
    private enum ECreatePosition
    {
        Self,
        Target,
    }

    [Header("Field Skill Option")]
    [SerializeField] private ECreatePosition _createPos = ECreatePosition.Self;
    [SerializeField] private bool _isOperateOnce = false;
    [Tooltip("얼마나 오래 갈건가?")]
    public float Duration = 2.0f;
    [SerializeField] private float _actionPeriod = 0.5f;
    private static readonly int EXPECT_FRAMERATE = 60;
    private int operateStack = 0;

    [Header("Field Sound Option")]
    [SerializeField] private AudioSource _fieldPlaySound = null;

    protected void Awake()
    {
        // 0으로 초기화되서 무한 호출되는 경우 방지
        if (_actionPeriod == 0)
        {
            _actionPeriod = 1f;
        }
    }

    protected override void InitObjectValue()
    {
        base.InitObjectValue();
        _lifeTime = Duration;
    }

    protected override void StartAfterInitSetting()
    {
        base.StartAfterInitSetting();

        if (_fieldPlaySound)
        {
            _fieldPlaySound.Play();
        }
    }

    private void FixedUpdate()
    {
        operateStack = (operateStack + 1) % GetFramePerActionPeriod();
    }

    private int GetFramePerActionPeriod()
    {
        return (int)(EXPECT_FRAMERATE * _actionPeriod);
    }

    private void OnTriggerStay(Collider other)
    {
        if (!_isOperateOnce && operateStack == 0 && !IsTriggeredRecently(other))
        {
            GetUseEffect(other);
        }
    }

    // 데미지 2번 줄 필요 없음
    protected override void OnTriggerEnter(Collider other)
    {
        if (_isOperateOnce)
        {
            base.OnTriggerEnter(other);
        }
        return;
    }

    public override void SetTransformInfo(Vector3 userPos, Vector3 targetPos)
    {
        if (_createPos == ECreatePosition.Self)
        {
            transform.position = userPos;
        }
        else if (_createPos == ECreatePosition.Target)
        {
            transform.position = targetPos;
        }
    }
}
