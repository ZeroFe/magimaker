﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CDashSkillBase : CHitObjectBase
{
    [Header("Dash Skill Option")]
    [SerializeField] private float _dashDistance = 5.0f;
    [SerializeField] private float _dashTime = 0.3f;
    private Vector3 _lookRotationVec3 = Vector3.zero;
    private static readonly float DASH_SPEED = 30;

    [Header("Dash Effect Option")]
    [SerializeField] private ParticleSystem _dashParticle = null;
    [SerializeField] private AudioSource _dashSound = null;

    protected override void InitObjectValue()
    {
        base.InitObjectValue();
        _lifeTime = 0.5f;
    }

    protected override void StartAfterInitSetting()
    {
        base.StartAfterInitSetting();
        StartCoroutine(nameof(Dash));
    }

    protected override void StartParticleSystems()
    {
        if (_dashParticle)
        {
            var main = _dashParticle.main;
            main.startSpeed = _dashDistance / _dashTime;
            main.startLifetime = _dashTime;
            _dashParticle.Play();
        }
    }

    public override void SetTransformInfo(Vector3 userPos, Vector3 targetPos)
    {
        var objectivePos = targetPos - userPos;
        objectivePos.y = 0;
        Quaternion lookRotation = Quaternion.LookRotation(objectivePos);
        transform.position = userPos + Vector3.up;
        transform.rotation = lookRotation;
    }

    IEnumerator Dash()
    {
        if (objectUser)
        {
            // 사용자 애니메이터가 applyRootMotion == true인 경우 velocity 조정이 먹히지 않음
            // 점프 상태로 만들어 다음 프레임에선 applyRootMotion == false가 되도록 하여 velocity 조정을 막는다
            objectUser.transform.position = objectUser.transform.position + Vector3.up;
            yield return new WaitForFixedUpdate();
            // 대쉬 시간에 따라 대쉬 거리만큼 이동하도록 변경 필요
            Vector3 dir = gameObject.transform.rotation * (Vector3.forward * (_dashDistance / _dashTime));
            dir.y = 0;
            objectUser.GetComponent<Rigidbody>().velocity = dir;
            gameObject.GetComponent<Rigidbody>().velocity = dir;
            yield return new WaitForSeconds(_dashTime);
            // 대쉬가 끝났으므로 멈춤
            objectUser.GetComponent<Rigidbody>().velocity = Vector3.zero;
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }
}
