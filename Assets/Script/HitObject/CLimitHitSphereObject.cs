﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// 번개구슬 구현을 위한 클래스
/// 다른 스킬 및 아이템 구현에 따라 클래스 기능 분리 예정
/// </summary>
public class CLimitHitSphereObject : CHitObjectBase
{
    [Header("Limit Hit Option")]
    [SerializeField] private int _hitLimit = 1;

    [SerializeField] private float _sphereRadius = 5f;
    [Tooltip("얼마나 오래 갈건가?"), SerializeField]
    private float Duration = 2.0f;
    [SerializeField] private float _actionPeriod = 0.5f;
    private static readonly int EXPECT_FRAMERATE = 60;
    private int operateStack = 0;

    [SerializeField] private LineRenderer _lightningEffect = null;
    private WaitForSeconds _lightningDrawWait = new WaitForSeconds(0.2f);
    private Vector3 _enemyPos = Vector3.zero;

    protected void Awake()
    {
        // 0으로 초기화되서 무한 호출되는 경우 방지
        if (_actionPeriod == 0)
        {
            _actionPeriod = 1f;
        }
    }

    protected override void InitObjectValue()
    {
        base.InitObjectValue();
        _lifeTime = Duration;
    }

    private void FixedUpdate()
    {
        operateStack = (operateStack + 1) % GetFramePerActionPeriod();

        if (operateStack != 0)
        {
            return;
        }

        int layerMask = (1 << LayerMask.NameToLayer("Monster"));
        Collider[] overlapedColliders = Physics.OverlapSphere(transform.position, _sphereRadius, layerMask);
        // Hit Limit보다 많은 대상이 있을 경우 먼저 찾은 대상부터 공격
        int hitCount = 0;
        for (int i = 0; i < overlapedColliders.Length; i++)
        {
            var colliderStat = overlapedColliders[i].GetComponent<CharacterPara>();
            if (colliderStat)
            {
                _enemyPos = overlapedColliders[i].transform.position + Vector3.up;
                colliderStat.TakeUseEffectHandleList(useEffects, objectUser);
                StartCoroutine(nameof(DrawAttackEffect));
                hitCount++;
            }
            if (hitCount >= _hitLimit)
            {
                return;
            }
        }
    }

    private int GetFramePerActionPeriod()
    {
        return (int)(EXPECT_FRAMERATE * _actionPeriod);
    }

    // 번개구슬용 이펙트 구현
    // 이후 공통된 효과가 생길 시 클래스 분리할 것
    IEnumerator DrawAttackEffect()
    {
        _lightningEffect.gameObject.SetActive(true);
        _lightningEffect.SetPosition(0, transform.position);
        _lightningEffect.SetPosition(1, _enemyPos);
        yield return _lightningDrawWait;
        _lightningEffect.gameObject.SetActive(false);
    }
}
