﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 모든 직선형 공격 클래스
 * 
 */
public class CProjectileBase : CHitObjectBase
{
    [Header("Projectile Option")]
    [Tooltip("투사체가 움직이는 속도"), SerializeField]
    private float _speed = 4.5f;
    [Tooltip("투사체 사정거리"), SerializeField]
    private float _range = 5f;

    [Tooltip("관통여부"), SerializeField]
    private bool isPenetrate = false;

    [Tooltip("투사체 없이 레이캐스트로 동작"), SerializeField]
    private bool isRaycast = false;

    [Header("Projectile Effect Option")]

    [Tooltip("충돌체를 그리는 파티클 시스템"), SerializeField]
    private ParticleSystem[] projectileParticles;

    [Tooltip("충돌체 발사 시 재생되는 소리"), SerializeField]
    private AudioSource projectileShootSound;

    [Tooltip("충돌 및 물리에 사용할 충돌체 개체")]
    public GameObject ProjectileColliderObject;

    [Header("Projectile Collision Effect")]

    [Tooltip("충돌 효과(파티클, 사운드, 데미지 등) 오브젝트"), SerializeField]
    private GameObject ProjectileCollisionEffectObject;

    private void SetVelocity(float speed)
    {
        Vector3 dir = ProjectileColliderObject.transform.rotation * (Vector3.forward * speed);
        ProjectileColliderObject.GetComponent<Rigidbody>().velocity = dir;

        if (projectileParticles != null)
        {
            foreach (var projectileParticle in projectileParticles)
            {
                var main = projectileParticle.main;
                main.startSpeed = speed;
            }
        }
    }

    protected override void StartParticleSystems()
    {
        foreach (ParticleSystem p in gameObject.GetComponentsInChildren<ParticleSystem>())
        {
            if (p.main.startDelay.constant == 0.0f)
            {
                // wait until next frame because the transform may change
                var m = p.main;
                var d = p.main.startDelay;
                d.constant = 0.01f;
                m.startDelay = d;
            }
            p.Clear();
            p.Simulate(p.main.duration);
            p.Play();
        }
    }

    protected override void OnEnable()
    {
        base.OnEnable();
    }

    protected override void InitObjectValue()
    {
        base.InitObjectValue();
        SetVelocity(_speed);
        _lifeTime = _range / _speed;
    }

    protected override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);

        var collisionObject = CSkillObjectPool.Instance.GetAvailableSkillObject(ProjectileCollisionEffectObject);
        collisionObject.GetComponent<CHitObjectBase>().SetObjectUser(objectUser);
        collisionObject.GetComponent<CHitObjectBase>().SetTransformInfo(transform.position, transform.position);
        collisionObject.SetActive(true);

        gameObject.SetActive(false);
    }

    public override void SetTransformInfo(Vector3 userPos, Vector3 targetPos)
    {
        var objectivePos = targetPos - userPos;
        Quaternion lookRotation = Quaternion.LookRotation(objectivePos);
        transform.position = userPos + Vector3.up;
        transform.rotation = lookRotation;
    }
}
