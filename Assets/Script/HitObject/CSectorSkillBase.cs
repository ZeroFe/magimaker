﻿using UnityEngine;

public class CSectorSkillBase : CHitObjectBase
{
    [System.Serializable]
    enum EOperateType
    {
        // 투사체 발사 : 부채꼴 각도를 기준으로 여러 갈래로 투사체 발사를 한다
        ProjectileMultipleShot,
        // 범위 충돌 : 범위 내 대상 모두에게 적용시킨다
        Area,
    }

    [Header("Sector Skill Option")]
    [Tooltip("투사체로 공격할지, 범위 내 전체 공격할지"), SerializeField]
    private EOperateType _operateType = EOperateType.Area;

    [Tooltip("사정 거리"), SerializeField]
    private float _range = 5f;

    [Tooltip("부채꼴 각도"), SerializeField]
    private float _degree = 30;

    #region Projectile property
    [Tooltip("투사체 발사 타입일 때 쏘는 투사체"), SerializeField]
    private GameObject _projectile_prefab = null;
    [Tooltip("투사체 발사 타입일 때 쏘는 투사체 개수"), SerializeField]
    private int _projectile_count = 1;

    private bool _isShootProjectile = false;

    private Vector3 _userPos = Vector3.zero;
    private Vector3 _targetPos = Vector3.zero;
    #endregion

    [Tooltip("범위 공격의 공격 간격"), SerializeField]
    private float _area_operatePeriod = 1f;

    private static readonly int EXPECT_FRAMERATE = 60;
    private int operateStack = 0;

    protected void Awake()
    {
        // 0으로 초기화되서 무한 호출되는 경우 방지
        if (_area_operatePeriod == 0)
        {
            _area_operatePeriod = 1f;
        }
    }

    protected override void OnEnable()
    {
        _isShootProjectile = false;
    }

    protected override void Update()
    {
        base.Update();

        if (IsInit && _operateType == EOperateType.ProjectileMultipleShot && !_isShootProjectile)
        {
            ShootProjectiles();
            _isShootProjectile = true;
        }
    }

    private void FixedUpdate()
    {
        operateStack = (operateStack + 1) % GetFramePerActionPeriod();
    }

    private int GetFramePerActionPeriod()
    {
        return (int)(EXPECT_FRAMERATE * _area_operatePeriod);
    }

    private void OnTriggerStay(Collider other)
    {
        if (operateStack == 0 && !IsTriggeredRecently(other))
        {
            GetUseEffect(other);
        }
    }

    // 데미지 2번 줄 필요 없음
    protected override void OnTriggerEnter(Collider other)
    {
        return;
    }

    private void ShootProjectiles()
    {
        if (_projectile_prefab == null)
        {
            return;
        }

        var leftHalfDegree = -_degree / 2;
        var projectileDegreeDiff = _degree / (_projectile_count - 1);
        for (int i = 0; i < _projectile_count; i++)
        {
            var projectileObj = CSkillObjectPool.Instance.GetAvailableSkillObject(_projectile_prefab);

            var hitObjectBase = projectileObj.GetComponent<CHitObjectBase>();
            hitObjectBase.SetObjectUser(objectUser);
            hitObjectBase.SetTransformInfo(_userPos, _targetPos);
            if (_projectile_count != 1)
            {
                hitObjectBase.RotateY(leftHalfDegree + i * projectileDegreeDiff);
            }

            projectileObj.SetActive(true);
        }
    }

    //public Vector3 PointOnCircle(float x, float z, Vector3 angle)
    //{
    //    float xPos = x + Mathf.Sin(angle.y * Mathf.PI / 180) * ColliderSet.arg2;
    //    float zPos = z + Mathf.Cos(angle.y * Mathf.PI / 180) * ColliderSet.arg2;
    //    Vector3 pos = new Vector3(xPos, 0, zPos);
    //    Debug.Log(gameObject.name);

    //    return pos;
    //}

    public override void SetTransformInfo(Vector3 userPos, Vector3 targetPos)
    {
        // 시작점은 userPos가 되고, 목표 방향은 targetPos가 된다
        _userPos = userPos;
        _targetPos = targetPos;
        // 시작점 피벗 설정 및 시작점 설정 코드 필요
        var objectivePos = targetPos - userPos;
        Quaternion lookRotation = Quaternion.LookRotation(objectivePos);
        transform.rotation = lookRotation;
    }
}
