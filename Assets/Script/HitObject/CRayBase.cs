﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CRayBase : CHitObjectBase
{
    [Header("Ray Option")]
    [SerializeField] private LineRenderer lineParticle = null;
    [SerializeField] private Collider _rayHitBox = null;
    [SerializeField] private float _actionPeriod = 0.5f;
    private static readonly int EXPECT_FRAMERATE = 60;
    private int operateStack = 0;

    private Vector3 _startPoint = Vector3.zero;
    private Vector3 _targetPoint = Vector3.one;

    [Header("Ray Hit Point Effect Option")]
    [SerializeField]
    private ParticleSystem _hitEffectParticle = null;

    protected override void InitObjectValue()
    {
        base.InitObjectValue();
        _lifeTime = CastingTime;
    }

    protected override void StartAfterInitSetting()
    {
        base.StartAfterInitSetting();
    }

    private void FixedUpdate()
    {
        operateStack = (operateStack + 1) % GetFramePerActionPeriod();
    }

    private int GetFramePerActionPeriod()
    {
        return (int)(EXPECT_FRAMERATE * _actionPeriod);
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        if (lineParticle)
        {
            lineParticle.SetPosition(0, _startPoint);
            lineParticle.SetPosition(1, _targetPoint);
        }
        if (_rayHitBox)
        {
            _rayHitBox.transform.position = _targetPoint;
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if (operateStack == 0 && !IsTriggeredRecently(other))
        {
            GetUseEffect(other);
        }
    }

    public override void SetTransformInfo(Vector3 userPos, Vector3 targetPos)
    {
        _startPoint = userPos;
        _targetPoint = targetPos;
    }
}
