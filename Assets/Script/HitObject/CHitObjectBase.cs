﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 스킬 및 아이템에서 사용할 수 있는 오브젝트 설정 클래스
/// 이 클래스에선 적용(공격) 대상 설정 및 캐스팅 방법만 정의하며, 작동 방식은 하위 클래스에서 설정함
/// </summary>
public class CHitObjectBase : MonoBehaviour
{
    #region Object Type
    public enum EHitTarget
    {
        Enemy,
        Allies,
    }

    public enum ECasting
    {
        FinishMotion,
        FinishTime,
        Focusing,
    }

    protected static string[] objectLayerNameArr = new string[] { "PlayerSkill", "MonsterSkill" };

    [Header("Skill Object Type Infomation")]
    [SerializeField] protected EHitTarget _hitTarget = EHitTarget.Enemy;

    public ECasting Casting { get { return _casting; } }
    [SerializeField] protected ECasting _casting = ECasting.FinishMotion;
    public float CastingTime { get { return _castingTime; } }
    [Tooltip("캐스팅에 걸리는 시간 혹은 최대 유지 시간")]
    [SerializeField] protected float _castingTime = 0.0f;
    #endregion

    [Header("Hit Effect")]
    [Tooltip("충돌 시 적용될 효과")]
    public List<CUseEffectHandle> useEffects;

    [Tooltip("시작 시, 한번 재생되는 오디오 소스")]
    public AudioSource hitSound;

    // 실제 지속시간
    protected float _lifeTime = 0.5f;

    private int _recentCollisionInstanceID = 0;

    public bool IsInit { get; set; }
    protected GameObject objectUser = null;

    private IEnumerator CleanupEverythingCoRoutine()
    {
        // 2 extra seconds just to make sure animation and graphics have finished ending
        yield return new WaitForSeconds(2.0f);

        gameObject.SetActive(false);
    }

    protected virtual void StartParticleSystems()
    {
        foreach (ParticleSystem p in gameObject.GetComponentsInChildren<ParticleSystem>())
        {
            if (p.main.startDelay.constant == 0.0f)
            {
                // wait until next frame because the transform may change
                var m = p.main;
                var d = p.main.startDelay;
                d.constant = 0.01f;
                m.startDelay = d;
            }
            p.Clear();
            p.Simulate(p.main.duration);
            p.Play();
        }
    }

    protected virtual void OnEnable()
    {
        InitObjectValue();
        StartAfterInitSetting();
    }

    /// <summary>
    /// 객체 생성(Active) 시 초기화할 값 등을 설정한다
    /// </summary>
    protected virtual void InitObjectValue()
    {
        IsInit = false;
    }

    /// <summary>
    /// 초기값 설정 후 파티클 시스템, 소리 등을 시작한다
    /// </summary>
    protected virtual void StartAfterInitSetting()
    {
        // start any particle system that is not in the list of manual start particle systems
        StartParticleSystems();
    }

    protected virtual void Update()
    {
        if (_lifeTime <= 0.0f)
        {
            Stop();
        }
        // reduce the duration
        _lifeTime -= Time.deltaTime;
    }

    private void OnDisable()
    {
        IsInit = false;
    }

    public static void CreateExplosion(Vector3 pos, float radius, float force)
    {
        if (force <= 0.0f || radius <= 0.0f)
        {
            return;
        }

        // find all colliders and add explosive force
        Collider[] objects = UnityEngine.Physics.OverlapSphere(pos, radius);
        foreach (Collider h in objects)
        {
            Rigidbody r = h.GetComponent<Rigidbody>();
            if (r != null)
            {
                r.AddExplosionForce(force, pos, radius);
            }
        }
    }

    public void CancelFocusingSkill()
    {
        if (Casting == ECasting.Focusing)
        {
            _lifeTime = 0.0f;
        }
    }

    public virtual void Stop()
    {
        //StartCoroutine(CleanupEverythingCoRoutine());
        gameObject.SetActive(false);
    }

    #region Object Setting
    /// <summary>
    /// 스킬(아이템)의 사용자를 설정한다
    /// 설정한 사용자 값에 맞춰 스킬(아이템)의 적용대상이나 능력치가 달라진다
    /// </summary>
    /// <param name="user"></param>
    public void SetObjectUser(GameObject user)
    {
        objectUser = user;

        gameObject.tag = user.tag;

        SetObjectLayer(user.layer);

        // 유저 스탯에 비례해 스킬 발사
        var userStat = user.GetComponent<CharacterPara>();
        foreach (var useEffect in useEffects)
        {
            useEffect.EnhanceEffectByStat(userStat);
        }
        IsInit = true;
    }

    public virtual void SetTransformInfo(Vector3 userPos, Vector3 targetPos)
    {
        var objectivePos = targetPos - userPos;
        Quaternion lookRotation = Quaternion.LookRotation(objectivePos);
        transform.position = userPos + Vector3.up;
        transform.rotation = lookRotation;
    }

    public void RotateY(float yDegree)
    {
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + Vector3.up * yDegree);
    }

    public virtual void SetObjectLayer(int userLayer)
    {
        int objectLayerNum;
        if (userLayer == LayerMask.NameToLayer("Player"))
        {
            objectLayerNum = 0;
        }
        else if (userLayer == LayerMask.NameToLayer("Monster"))
        {
            objectLayerNum = 1;
        }
        else if (userLayer == LayerMask.NameToLayer("DeadBody"))
        {
            objectLayerNum = GetLayerNumByTag();
            if (objectLayerNum == -1)
            {
                return;
            }
        }
        else
        {
            return;
        }

        objectLayerNum ^= (int)_hitTarget;
        gameObject.layer = LayerMask.NameToLayer(objectLayerNameArr[objectLayerNum]);
    }

    protected int GetLayerNumByTag()
    {
        if (tag == "Player" || tag == "Allies")
        {
            return 0;
        }
        else if (tag == "Monster")
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }
    #endregion

    #region Hit Implementation
    protected virtual void OnTriggerEnter(Collider other)
    {
        if (!IsTriggeredRecently(other))
        {
            GetUseEffect(other);
        }
    }

    protected bool IsTriggeredRecently(Collider other)
    {
        int recentCollisionID = other.gameObject.GetInstanceID();
        if (_recentCollisionInstanceID == recentCollisionID)
        {
            return true;
        }
        return false;
    }

    protected void GetUseEffect(Collider other)
    {
        var cPara = other.GetComponent<CharacterPara>();
        if (cPara != null)
        {
            cPara.TakeUseEffectHandleList(useEffects, objectUser);
        }
    }
    #endregion
}
