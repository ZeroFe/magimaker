﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CRemoteHitBox : MonoBehaviour
{
    [SerializeField] private CRayBase _rayObject = null;

    private void OnTriggerStay(Collider other)
    {
        if (_rayObject)
        {
            _rayObject.OnTriggerStay(other);
        }
    }
}
