﻿using UnityEngine;

/// <summary>
/// 오직 스킬 사용자만에게만 효과를 주는 스킬에 사용하는 클래스
/// 아군이 스킬 사용자와 겹쳐있어도 스킬 사용자만 효과를 받게 하기 위함
/// </summary>
public class CHitUserSelfObject : CHitObjectBase
{
    protected override void StartAfterInitSetting()
    {
        base.StartAfterInitSetting();
        
        if (objectUser && objectUser.GetComponent<CharacterPara>())
        {
            objectUser.GetComponent<CharacterPara>().TakeUseEffectHandleList(useEffects, objectUser);
        }
    }
}
