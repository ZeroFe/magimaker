﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 동시에 여러 HitObject를 생성하는 오브젝트
/// 아군 / 적군에게 서로 다른 효과를 주는 등 레이어나 효과를 분리해서 줄 수 있음 
/// ex) 정화의 빛, 헬파이어
/// </summary>
public class CSeparatedHitObject : CHitObjectBase
{
    [Header("Seperated Hit Option")]
    [SerializeField] private List<CHitObjectBase> _childHitObjects = new List<CHitObjectBase>();
    [SerializeField] private float _duration = 5.0f;
    [SerializeField] private ParticleSystem _commonEffect = null;

    private Vector3 _userPos = Vector3.zero;
    private Vector3 _targetPos = Vector3.zero;

    protected override void InitObjectValue()
    {
        base.InitObjectValue();
        _lifeTime = _duration;
        InitChildHitObjects();
    }

    protected override void StartAfterInitSetting()
    {
        base.StartAfterInitSetting();
    }

    /// <summary>
    /// 사용자 정보 등을 하위 HitObject들에 전달한다
    /// </summary>
    private void InitChildHitObjects()
    {
        foreach (var childHitObject in _childHitObjects)
        {
            if (objectUser)
            {
                childHitObject.SetObjectUser(objectUser);
            }
            childHitObject.SetTransformInfo(_userPos, _targetPos);

            childHitObject.gameObject.SetActive(true);
        }
    }

    public override void SetTransformInfo(Vector3 userPos, Vector3 targetPos)
    {
        base.SetTransformInfo(userPos, targetPos);
        _userPos = userPos;
        _targetPos = targetPos;
    }
}
