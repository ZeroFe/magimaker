﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class COptionWindow : DestroyableSingleton<COptionWindow>
{
    #region 사운드 조절 관련
    [SerializeField] private Slider _wholeSoundVolumeSlider = null;

    private void InitSound()
    {
        _wholeSoundVolumeSlider.value = AudioListener.volume;
        _wholeSoundVolumeSlider.onValueChanged.AddListener(SetWholeSoundVolume);
    }

    private void SetWholeSoundVolume(float value)
    {
        AudioListener.volume = value;
    }
    #endregion

    private void Start()
    {
        InitSound();
    }
}
