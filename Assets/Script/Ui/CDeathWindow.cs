﻿using UnityEngine;
using UnityEngine.UI;

public class CDeathWindow : Singleton<CDeathWindow>
{
    [SerializeField] private Button ObserverButton = null;
    [SerializeField] private Button ExitButton = null;

    public void Awake()
    {
        if (ObserverButton)
        {

        }
        if (ExitButton)
        {
            ExitButton.onClick.AddListener(CInGameQuitProcedure.Instance.QuitInGame);
        }

        gameObject.SetActive(false);
    }
}
