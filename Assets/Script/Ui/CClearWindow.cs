﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CClearWindow : DestroyableSingleton<CClearWindow>
{
    [SerializeField] private Button QuitToLobbyBtn = null;

    private void Awake()
    {
        QuitToLobbyBtn.onClick.AddListener(CInGameQuitProcedure.Instance.QuitInGame);
    }
}
