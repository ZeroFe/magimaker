﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CCastingGaugeDrawer : MonoBehaviour
{
    [Header("UI Member Setting")]
    [SerializeField] private TMPro.TMP_Text _castingSkillNameText = null;
    [SerializeField] private Image _castingFillImage = null;
    [SerializeField] private TMPro.TMP_Text _castingTimeText = null;

    [Header("Animation Option")]
    [SerializeField] private float _cancelFadeTime = 0.3f;

    private float _maxCastingTime = 0.0f;
    private float _currentCastingTime = 0.0f;

    private static string CASTING_CANCEL_ALERT_TEXT = "중단됨";
    
    public static CCastingGaugeDrawer Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        gameObject.SetActive(false);
    }

    public void StartCast(string skillName, float castingTime)
    {
        gameObject.SetActive(true);
        _castingSkillNameText.text = skillName;
        _maxCastingTime = castingTime;
        StopCoroutine(nameof(DrawCastingCancelState));
        StartCoroutine(nameof(DrawCastingTime));
    }

    public void CancelCast()
    {
        if (!gameObject.activeSelf)
        {
            return;
        }

        StopCoroutine(nameof(DrawCastingTime));
        StartCoroutine(nameof(DrawCastingCancelState));
    }

    IEnumerator DrawCastingTime()
    {
        _currentCastingTime = _maxCastingTime;
        while(_currentCastingTime > 0)
        {
            yield return new WaitForSeconds(0.05f);
            _currentCastingTime -= 0.05f;
            _currentCastingTime = (float)(System.Math.Truncate(_currentCastingTime * 10) * 0.1);
            _castingTimeText.text = $"{_currentCastingTime} / {_maxCastingTime}";
            _castingFillImage.fillAmount = _currentCastingTime / _maxCastingTime;
        }
        gameObject.SetActive(false);
    }

    IEnumerator DrawCastingCancelState()
    {
        _castingTimeText.text = CASTING_CANCEL_ALERT_TEXT;
        yield return new WaitForSeconds(_cancelFadeTime);
        gameObject.SetActive(false);
    }
}
