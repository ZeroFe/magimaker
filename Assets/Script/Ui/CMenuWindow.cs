﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CMenuWindow : DestroyableSingleton<CMenuWindow>
{
    public Button QuitToLobbyBtn = null;
    public Button ReturnToGameBtn = null;
    public Button HelpBtn = null;
    public Button OptionBtn = null;

    private void Awake()
    {
        QuitToLobbyBtn.onClick.AddListener(CInGameQuitProcedure.Instance.QuitInGame);
        if (OptionBtn)
        {
            OptionBtn.onClick.AddListener(CWindowFacade.instance.OpenOptionWindow);
        }
    }
}
