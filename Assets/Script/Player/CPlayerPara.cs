﻿using UnityEngine;
using System.Collections;

public class CPlayerPara : CharacterPara
{
    public string _name;
    public bool _invincibility;
    public bool _invincibilityChecker;
    public Animator _myAnimator;
    public float _runAnimationMultiply = 1f;
    public Renderer _obj;
    Color _originColor;

    [SerializeField]
    public CInventory Inventory;

    //debug용
    public static CPlayerPara instance = null;

    #region Property
    public override int TotalAttackMin
    {
        get
        {
            Debug.Log("AttackMin = " + (int)((_attackMin
                + Inventory.GetEquipAbilityIncreaseSize(Item.EEquipAbility.Attack)
                + _attackMin * Inventory.GetEquipAbilityIncreaseSize(Item.EEquipAbility.AttackPercent) / 100)
              * _buffCoef[(int)EBuffAbility.Attack] * _debuffCoef[(int)EBuffAbility.Attack]));

            return (int)((_attackMin + Inventory.GetEquipAbilityIncreaseSize(Item.EEquipAbility.Attack)
                + _attackMin * Inventory.GetEquipAbilityIncreaseSize(Item.EEquipAbility.AttackPercent) / 100)
              * _buffCoef[(int)EBuffAbility.Attack] * _debuffCoef[(int)EBuffAbility.Attack]);
        }
    }
    // ((캐릭터 공격력 * 공격력 증가) + 장비 공격력) * 버프로 올라가는 공격력 %
    public override int TotalAttackMax
    {
        get
        {
            return (int)((_attackMax + Inventory.GetEquipAbilityIncreaseSize(Item.EEquipAbility.Attack)
                + _attackMax * Inventory.GetEquipAbilityIncreaseSize(Item.EEquipAbility.AttackPercent) / 100)
              * _buffCoef[(int)EBuffAbility.Attack] * _debuffCoef[(int)EBuffAbility.Attack]);
        }
    }
    public override int TotalDefenece
    {
        get
        {
            return (int)(_defense + Inventory.GetEquipAbilityIncreaseSize(Item.EEquipAbility.Defence)
              * _buffCoef[(int)EBuffAbility.Defence] * _debuffCoef[(int)EBuffAbility.Defence]);
        }
    }
    public override int TotalMaxHp
    {
        get { return (int)(_maxHp + Inventory.GetEquipAbilityIncreaseSize(Item.EEquipAbility.MaxHp)); }
    }
    public int TotalHpRegen
    {
        get { return (int)(Inventory.GetEquipAbilityIncreaseSize(Item.EEquipAbility.HpRegen)); }
    }
    public override int CurrentHp
    {
        protected set
        {
            if (value < 0)
            {
                _curHp = 0;
                if (!_isDead)
                {
                    Debug.Log("Dead?");
                    _isDead = true;
                    DeadEvent.Invoke();
                }
            }
            else if (value > TotalMaxHp)
            {
                _curHp = TotalMaxHp;
            }
            else
            {
                if (_invincibility) return;
                _curHp = value;
            }
            print(name + "'s HP: " + CurrentHp);
            HpDrawing?.Invoke(CurrentHp, TotalMaxHp);
            _myAnimator.SetInteger("Hp", CurrentHp);
        }
    }
    #endregion

    #region Events
    public UnityEngine.Events.UnityEvent TimeEvent = new UnityEngine.Events.UnityEvent();
    #endregion

    protected override void Awake()
    {
        base.Awake();
        Inventory = new CInventory(gameObject);
    }

    protected override void Start()
    {
        base.Start();
        StartCoroutine(OnTime());
    }

    public override void InitPara()
    {
        _maxHp = 1000;
        _curHp = _maxHp;
        _attackMin = 50;
        _attackMax = 80;
        _defense = 30;
        _isAnotherAction = false;
        _isStunned = false;
        _isDead = false;
        _invincibility = false;
        //_originColor = _obj.material.color;
        _myAnimator = GetComponent<Animator>();
        _myAnimator.SetInteger("Hp", _curHp);
    }

    public override void DamegedRegardDefence(int enemyAttack, GameObject giver = null)
    {
        int damage = enemyAttack * 1000 / (950 + 10 * TotalDefenece);
        float damageF = damage * ((100 + Inventory.GetEquipAbilityIncreaseSize(Item.EEquipAbility.DamageReduceRate)) / 100);
        DamagedDisregardDefence((int)damageF, null);
    }

    #region 무적판정
    public void OffInvincibility()
    {
        _invincibility = false;
        StopCoroutine(PowerOverwhelming());
        _obj.material.color = _originColor;
        gameObject.layer = LayerMask.NameToLayer("Player");
    }

    public void OnInvincibility()
    {
        if (_invincibilityChecker)
        {
            gameObject.layer = LayerMask.NameToLayer("DeadBody");
            _invincibilityChecker = false;
            StartCoroutine(PowerOverwhelming());
        }
    }

    IEnumerator PowerOverwhelming()
    {
        while (_invincibility)
        {
            float flicker = Mathf.Abs(Mathf.Sin(Time.time * 10));
            _obj.material.color = _originColor * flicker;
            yield return null;
        }
    }

    private void Update()
    {
        _myAnimator.SetFloat("RunMulti", _runAnimationMultiply);
    }

    #endregion

    #region 시간마다 발동하는 이벤트
    IEnumerator OnTime()
    {
        var waitTime = new WaitForSeconds(1);
        while (true)
        {
            yield return waitTime;
            TimeEvent?.Invoke();
        }
    }
    #endregion
}