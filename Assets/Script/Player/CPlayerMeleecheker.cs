﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;

public class CPlayerMeleecheker : MonoBehaviour
{
    [Tooltip("충돌 시 적용될 효과")]
    public List<CUseEffectHandle> useEffects;

    bool _exist;
    CPlayerPara _myPara;
    List<int> _attackedMonster = new List<int>();

    void Start()
    {
        _myPara = transform.parent.GetComponent<CPlayerPara>();
    }

    private void OnTriggerEnter(Collider other)
    {
        _exist = false;

        if (other.GetComponent<CharacterPara>() != null)
        {
            GetComponent<AudioSource>().Play();
            other.GetComponent<CharacterPara>().TakeUseEffectHandleList(useEffects, _myPara.gameObject);
        }
        //other.GetComponent<CharacterPara>()?.DamegedRegardDefence(_myPara.RandomAttackDamage(), _myPara.gameObject);
        //if (other.tag == "Monster" || other.tag == "Boss")
        //{
        //    for (int i = 0; i < _attackedMonster.Count; i++)
        //    {
        //        //Debug.Log(_attackedPlayer[i]);
        //        if (other.GetComponent<CEnemyPara>()._spawnID == _attackedMonster[i])
        //        {
        //            _exist = true;
        //        }
        //    }
        //    if (!_exist)
        //    {
        //        _attackedMonster.Add(other.GetComponent<CEnemyPara>()._spawnID);
        //        var para = other.GetComponent<CEnemyPara>();
        //        string tempstr = Regex.Replace(transform.root.gameObject.name, @"\D", "");
        //        int rstInt = int.Parse(tempstr);
        //        para.DamegedRegardDefence(_myPara.RandomAttackDamage(), rstInt);
        //    }
        //}
    }

    public void DiscardList()
    {
        _attackedMonster.Clear();
    }
}
