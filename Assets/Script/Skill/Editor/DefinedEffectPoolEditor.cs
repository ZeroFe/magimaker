﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(CDefinedEffectPool))]
public class DefinedEffectPoolEditor : Editor
{
    ReorderableList reorderableList;
    SerializedProperty unknownEffect;

    void OnEnable()
    {
        var prop = serializedObject.FindProperty("_definedEffects");

        reorderableList = new ReorderableList(serializedObject, prop);
        reorderableList.elementHeight = 84;
        reorderableList.drawElementCallback =
          (rect, index, isActive, isFocused) => {
              var element = prop.GetArrayElementAtIndex(index);
              rect.height -= 4;
              rect.y += 2;
              EditorGUI.PropertyField(rect, element);
          };

        var defaultColor = GUI.backgroundColor;

        reorderableList.drawHeaderCallback = (rect) =>
          EditorGUI.LabelField(rect, prop.displayName);

        unknownEffect = serializedObject.FindProperty("_undefinedEffect");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        reorderableList.DoLayoutList();
        EditorGUILayout.LabelField("Undefined Effect Setting");
        EditorGUILayout.PropertyField(unknownEffect, GUILayout.Height(80));
        serializedObject.ApplyModifiedProperties();
    }
}
