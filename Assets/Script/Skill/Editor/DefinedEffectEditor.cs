﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(DefinedEffect))]
public class DefinedEffectEditor : PropertyDrawer
{
    private DefinedEffect _definedEffect;

    private static readonly int nameSize = 200;
    private static readonly int space = 3;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        //원래는 1개의 프로퍼티인 것을 나타내기 위해서 PropertyScope로 둘러쌉니다
        using (new EditorGUI.PropertyScope(position, label, property))
        {
            position.height = EditorGUIUtility.singleLineHeight;

            var halfWidth = position.width * 0.5f;

            var nameRect = new Rect(position)
            {
                width = 200
            };

            var idRect = new Rect(nameRect)
            {
                x = nameRect.x + nameRect.width + space,
                width = position.width - nameRect.width - space,
            };

            //각 프로퍼티의 Rect을 구합니다
            var iconRect = new Rect(nameRect)
            {
                y = position.y + EditorGUIUtility.singleLineHeight + 2,
                width = 64,
                height = 64
            };

            var useEffectRect = new Rect(position)
            {
                x = position.x + 64 + space,
                y = idRect.y + EditorGUIUtility.singleLineHeight + 2,
                width = position.width - 64 - space
            };

            var particleEffectRect = new Rect(useEffectRect)
            {
                y = useEffectRect.y + EditorGUIUtility.singleLineHeight + 2
            };

            //각 프로퍼티의 SerializedProperty를 구합니다
            var iconProperty = property.FindPropertyRelative("icon");
            var nameProperty = property.FindPropertyRelative("name");
            var idProperty = property.FindPropertyRelative("ID");
            var useEffectProperty = property.FindPropertyRelative("useEffect");
            var particleEffectProperty = property.FindPropertyRelative("particleEffect");

            EditorGUIUtility.labelWidth = 64;
            EditorGUI.PropertyField(nameRect, nameProperty);
            EditorGUIUtility.labelWidth = 32;
            EditorGUI.PropertyField(idRect, idProperty);
            EditorGUIUtility.labelWidth = 80;

            //각 프로퍼티의 GUI을 표시
            iconProperty.objectReferenceValue =
              EditorGUI.ObjectField(iconRect, iconProperty.objectReferenceValue, typeof(Sprite), false);
            EditorGUI.PropertyField(useEffectRect, useEffectProperty);
            EditorGUI.PropertyField(particleEffectRect, particleEffectProperty);
        }
    }
}
