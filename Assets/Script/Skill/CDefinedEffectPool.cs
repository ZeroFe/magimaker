﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class DefinedEffect
{
    public Sprite icon;
    public string name;
    public int ID = 0;
    public CUseEffect useEffect = null;
    public GameObject particleEffect = null;
}

public class CDefinedEffectPool : DestroyableSingleton<CDefinedEffectPool>
{
    [SerializeField]
    private List<DefinedEffect> _definedEffects = new List<DefinedEffect>(3);
    // 따로 값을 설정하지 않았을 때 나오는 이미지, 이펙트 등
    [SerializeField]
    private DefinedEffect _undefinedEffect = new DefinedEffect();

    // ID에 맞는 Effect Particle을 구한다
    private Dictionary<int, List<GameObject>> _effectParticleDict = new Dictionary<int, List<GameObject>>();

    private void Start()
    {
        Init();
    }

    private void Init()
    {
        for (int i = 0; i < _definedEffects.Count; i++)
        {
            var effectList = new List<GameObject>();
            var effectID = _definedEffects[i].ID;
            _effectParticleDict.Add(effectID, effectList);
            AddDefinedEffectParticle(effectID);
        }
    }

    /// <summary>
    /// 지정된 효과에 맞는 UseEffect 설정값을 가져온다
    /// </summary>
    /// <param name="definedEffect">첨습, 화상 등 지정된 효과</param>
    /// <returns>지정된 효과에 맞는 UseEffect 설정값</returns>
    public CUseEffect GetDefinedUseEffectByIndex(int index)
    {
        return _definedEffects[index].useEffect;
    }

    #region 이펙트 관리
    private GameObject AddDefinedEffectParticle(int effectID)
    {
        if (_effectParticleDict.TryGetValue(effectID, out var definedEffectParticles))
        {

            var findedEffect = _definedEffects.Find(x => x.ID == effectID);
            if (findedEffect == null || findedEffect.particleEffect == null)
            {
                Debug.Log("Finded Effect is null or particle not settinged");
                return null;
            }
            var particleObject = Instantiate(findedEffect.particleEffect);
            particleObject.transform.SetParent(transform);
            definedEffectParticles.Add(particleObject);
            particleObject.SetActive(false);
            return particleObject;
        }
        else
        {
            Debug.Log("error : can't find effect");
            return null;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="id">첨습, 화상 등 지정된 효과</param>
    /// <returns>해당 효과에 맞는 시각효과</returns>
    public GameObject GetAvailableEffectParticle(int id)
    {
        GameObject skillObject;
        if (_effectParticleDict.TryGetValue(id, out var skillObjectList)
            && (skillObject = skillObjectList.Find(obj => obj.activeSelf == false)) != null)
        {
            Debug.Log("Find Available Effect");
            return skillObject;
        }
        return AddDefinedEffectParticle(id);
    }
    #endregion

    #region UI 관리
    public Sprite GetEffectIcon(int id)
    {
        var findingEffect = _definedEffects.Find(x => x.ID == id);
        return findingEffect != null ? findingEffect.icon : _undefinedEffect.icon;
    }
    #endregion
}
