﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class CEnemyPara : CharacterPara
{
    [System.Serializable]
    public class SkillType
    {
        [Tooltip("스킬 공격력 : 기본 공격력에서의 배수로 해둠")]
        public int SkillPower;
        [Tooltip("CC 종류 기본 값 None")]
        public CrowdControl CCType;
        public CrowdControlLevel CCLevel;
    }

    [HideInInspector] public Stack<int> _attacker = new Stack<int>();
    [Header ("Monster가 가지고 있는 스킬 속성 정의")]
    public List<SkillType> _skillType = new List<SkillType>();
    string _originTag = "Monster";
    [HideInInspector] public GameObject _myRespawn;
    Vector3 _originPos;
    public string _name;
    public int MonsterID { get; set; } = 0;

    #region 몬스터 피격 처리
    public class MonsterHitEvent : UnityEvent<CEnemyPara> { }
    public MonsterHitEvent monsterHitEvent { get; private set; } = new MonsterHitEvent();

    public override void TakeUseEffect(CUseEffect effect, GameObject giver)
    {
        monsterHitEvent.Invoke(this);
        base.TakeUseEffect(effect, giver);
    }

    public override void DamegedRegardDefence(int enemyAttack, GameObject giver)
    {
        monsterHitEvent.Invoke(this);
        base.DamegedRegardDefence(enemyAttack, giver);
    }

    private void OnEnable()
    {
        _spawnID = CMonsterManager.instance.AddMonsterInfo(gameObject);
        DeadEvent.AddListener(SetOffMonster);
    }
    #endregion

    #region 몬스터 데미지 동기화
    public CMonsterManager.MonsterTakeDamageEvent MonsterTakeDamage { get; private set; } = new CMonsterManager.MonsterTakeDamageEvent();
    public CMonsterManager.MonsterTakePersistEvent MonsterTakePersist { get; private set; } = new CMonsterManager.MonsterTakePersistEvent();

    public override void DamagedDisregardDefence(int enemyAttack, GameObject giver)
    {
        MonsterTakeDamage.Invoke(MonsterID, enemyAttack);
        base.DamagedDisregardDefence(enemyAttack, giver);
    }

    public override void ApplyPersistEffect(CUseEffect.PersistEffect persistEffect)
    {
        if (persistEffect != null && persistEffect.IsValid())
        {
            MonsterTakePersist.Invoke(MonsterID, persistEffect);
        }
        base.ApplyPersistEffect(persistEffect);
    }
    #endregion

    public override void InitPara()
    {
        base.InitPara();
        _isStunned = false;
        _isDead = false;
        _curHp = _maxHp;
    }
    
    public void SetRespawn(GameObject respawn, int spawnID, Vector3 originPos)
    {
        _myRespawn = respawn;
        _spawnID = spawnID;
        _originPos = originPos;
        //Debug.Log("My Respawn is : " + _myRespawn + "  My SpawnID is : " + _spawnID
        //    + "  My originPos is : " + _originPos);
    }

    // 방어력 계산식: 1000 / (950 + 10*방어력)
    public void DamegedRegardDefence(int enemyAttack, int attackEnemy)
    {
        int damage = enemyAttack * 1000 / (950 + 10 * TotalDefenece);
        _attacker.Push(attackEnemy);
        DamagedDisregardDefence(damage, null);
    }

    public void SetOffMonster()
    {
        Invoke("SetActiveFalse", 2f);
    }

    public void SetActiveFalse()
    {
        CMonsterManager.instance.RemoveMonster(_spawnID);
        gameObject.SetActive(false);
    }

    public void respawnAgain()
    {
        //  리스폰 오브젝트에서 처음 생성될때의 위치와 같게 함
        transform.position = _originPos;
        tag = _originTag;
        gameObject.layer = LayerMask.NameToLayer("Monster");
        InitPara();
    }

    #region MonsterManager를 통한 강제 UseEffect 실행
    public void ForceDamage(int damageAmount)
    {
        base.DamagedDisregardDefence(damageAmount, null);
    }

    public void ForcePersist(CUseEffect.PersistEffect persistEffect)
    {
        base.ApplyPersistEffect(persistEffect);
    }
    #endregion
}