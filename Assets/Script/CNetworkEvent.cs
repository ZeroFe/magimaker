﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Network
{
    [DisallowMultipleComponent]
    public class CNetworkEvent : MonoBehaviour
    {
        public class ChangingMoneyEvent : UnityEvent<int> { }

        public ChangingMoneyEvent EarnMoneyEvent = new ChangingMoneyEvent();
        public ChangingMoneyEvent LoseMoneyEvent = new ChangingMoneyEvent();

        public Action UsePortalEvent;
        public Action<int> PortalVoteEvent;

        public UnityEvent<int> EventRoomPopUpChooseEvent;

        public static CNetworkEvent instance;

        private CTcpClient _tcpManager;
        private CPlayerCommand _playerCommand;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
        }

        private void Start()
        {
            _tcpManager = CTcpClient.instance;
            _playerCommand = CPlayerCommand.instance;

            //if (_tcpManager?.IsConnect == true && !CClientInfo.IsSinglePlay())
            if (_tcpManager?.IsConnect == true)
            {
                _tcpManager.SetPacketInterpret(Network.CPacketInterpreter.PacketInterpret);

                Debug.Log("Multiplay Mode");
                AddNetworkCode();
                InitMultiplay();
            }
            else
            {
                AddSingleplayCode();
                InitSinglePlay();
            }
        }

        private void OnApplicationQuit()
        {
            if (_tcpManager?.IsConnect == true)
            {
                _tcpManager.SendShutdown(true);
            }
        }

        public void QuitPlayer(int playerNumber)
        {
            CPlayerCommand.instance.DeactivatePlayer(playerNumber);

            if (CPlayerCommand.instance.ActivatedPlayersCount == 1)
            {
                RemoveNetworkCode();
            }
        }

        private void InitMultiplay()
        {
            // 캐릭터 설정
            Debug.Log($"Set Character : Send Message");
            _playerCommand.SetActivePlayers(CClientInfo.PlayerCount);
            SendLodingFinish();
            SendCharacterInfoRequest();
        }

        private void InitSinglePlay()
        {
            Debug.Log("Singleplay Mode");
            _playerCommand.SetActivePlayers(1);
            _playerCommand.SetMyCharacter(0);
            CWaitingLoadViewer.Instance.FinishLoading();
            CPortalManager.instance.ExecuteEnterNextRoom(CCreateMap.CENTER_ROOM_POS);
        }

        private void AddNetworkCode()
        {
            var controller = CController.instance;
            // 플레이어 움직임
            controller.PlayerMoveEvent.AddListener(SendMoveStart);
            controller.PlayerPosCorrectionEvent.AddListener(SendMoveStop);
            controller.PlayerJumpEvent.AddListener(SendJumpStart);
            controller.PlayerAttackEvent.AddListener(SendAttackStart);
            controller.PlayerRollEvent.AddListener(SendRollStart);
            controller.PlayerSkillEvent.AddListener(SendActionStart);

            // 플레이어에게 UseEffect 전달
            controller.PlayerTakeDamageEvent.AddListener(SendTakeDamage);
            controller.PlayerTakeHealEvent.AddListener(SendTakeHeal);
            controller.PlayerTakePersistEffectEvent.AddListener(SendTakePersistEffect);
            //controller.PlayerGiveDamageEvent.AddListener(SendGiveDamage);
            //controller.PlayerGivePersistEffectEvent.AddListener(SendGivePersistEffect);

            // 몬스터에게 UseEffect 전달
            var monsterManager = CMonsterManager.instance;
            monsterManager.MonsterTakeDamage.AddListener(SendGiveDamage);
            monsterManager.MonsterTakePersist.AddListener(SendGivePersistEffect);

            // 아이템 획드
            controller.PlayerEarnItem.AddListener(SendEarnItem);

            // 포탈 관련
            UsePortalEvent += SendUsePortal;
            PortalVoteEvent += SendPortalVote;

            if (CClientInfo.JoinRoom.IsHost)
            {
                Debug.Log("I'm Host");
                AddNetworkHostCode();
            }
        }

        private void RemoveNetworkCode()
        {
            var controller = CController.instance;

            // 플레이어 움직임
            controller.PlayerMoveEvent.RemoveListener(SendMoveStart);
            controller.PlayerPosCorrectionEvent.RemoveListener(SendMoveStop);
            controller.PlayerJumpEvent.RemoveListener(SendJumpStart);
            controller.PlayerAttackEvent.RemoveListener(SendAttackStart);
            controller.PlayerRollEvent.RemoveListener(SendRollStart);
            controller.PlayerSkillEvent.RemoveListener(SendActionStart);

            // 포탈 관련
            UsePortalEvent -= SendUsePortal;
            PortalVoteEvent -= SendPortalVote;
        }

        private void AddNetworkHostCode()
        {
            // 몬스터 패턴 코드
            // 돈 획득
            // 방 생성
            //CPortalManager.instance.WaitEnterRoom.AddListener(SendWaitEntering);
            CPortalManager.instance.EnterNextRoom.AddListener(SendEnterNextRoom);
            CPortalManager.instance.EnterNextStage.AddListener(SendEnterNextStage);
        }

        private void AddSingleplayCode()
        {
            // 테스트용
            var monsterManager = CMonsterManager.instance;
            monsterManager.MonsterTakeDamage.AddListener(MonsterTakeDamageTest);
            //monsterManager.MonsterTakePersist.AddListener();

            // 몬스터 패턴 바로 적용하는 코드
            // 돈 획득 바로하는 코드
            //EarnMoneyEvent.AddListener(_playerCommand.EarnMoneyAllCharacter);
            // 포탈 바로 이동하는 코드
            //CPortalManager.instance.WaitEnterRoom.AddListener(CPortalManager.instance.WaitEntering);
            CPortalManager.instance.EnterNextRoom.AddListener(CPortalManager.instance.MoveToNextRoom);
            CPortalManager.instance.EnterNextStage.AddListener(CCreateMap.instance.EnterNextStageSingle);
        }

        private void MonsterTakeDamageTest(int monsterID, int damageAmount)
        {
            Debug.Log($"monster{monsterID} damaged {damageAmount}");
        }

        private void SucceedHost()
        {

        }

        #region Packet Send

        public static void SendCharacterInfoRequest()
        {
            var message = CPacketFactory.CreateCharacterInfoPacket();

            CTcpClient.instance.Send(message.data);
        }

        public static void SendMoveStart(Vector3 now, Vector3 dest)
        {
            var message = CPacketFactory.CreateMoveStartPacket(now, dest);

            CTcpClient.instance.Send(message.data);
        }

        public static void SendMoveStop(Vector3 now)
        {
            var message = CPacketFactory.CreateMoveStopPacket(now);

            CTcpClient.instance.Send(message.data);
        }

        public static void SendActionStart(int actionNumber, Vector3 now, Vector3 dest)
        {
            var message = CPacketFactory.CreateActionStartPacket(actionNumber, now, dest);

            CTcpClient.instance.Send(message.data);
        }

        private static void SendJumpStart(Vector3 currentPos, float rotate, bool isMoving)
        {
            Debug.Log("send jump");
            var message = CPacketFactory.CreateJumpStartPacket(currentPos, rotate, isMoving);
            CTcpClient.instance.Send(message.data);
        }

        private static void SendAttackStart(Vector3 currentPos, float rotate)
        {
            Debug.Log("send attack");
            var message = CPacketFactory.CreateAttackStartPacket(currentPos, rotate);
            CTcpClient.instance.Send(message.data);
        }

        private static void SendRollStart(Vector3 currentPos, float rotate)
        {
            Debug.Log("send roll");
            var message = CPacketFactory.CreateRollStartPacket(currentPos, rotate);
            CTcpClient.instance.Send(message.data);
        }

        #region UseEffect 동기화
        private static void SendTakeDamage(int damageAmount, int myID)
        {
            Debug.Log($"Send Take Damage : {damageAmount}");
            var message = CPacketFactory.CreateTakeDamage(damageAmount);
            CTcpClient.instance.Send(message.data);
        }

        private static void SendTakeHeal(int healAmount, int myID)
        {
            Debug.Log($"Send Take Heal : {healAmount}");
            var message = CPacketFactory.CreateTakeHeal(healAmount);
            CTcpClient.instance.Send(message.data);
        }

        private static void SendTakePersistEffect(CUseEffect.PersistEffect persistEffect, int myID)
        {
            Debug.Log($"Send Take persist Effect : id - {persistEffect.id}, time - {persistEffect.time}");
            var message = CPacketFactory.CreateTakePersistEffect(persistEffect);
            CTcpClient.instance.Send(message.data);
        }

        private static void SendGiveDamage(int monsterID, int damageAmount)
        {
            Debug.Log($"Send Give Damage - {damageAmount} to {monsterID}");
            var message = CPacketFactory.CreateGiveDamage(monsterID, damageAmount);
            CTcpClient.instance.Send(message.data);
        }

        private static void SendGivePersistEffect(int monsterID, CUseEffect.PersistEffect persistEffect)
        {
            Debug.Log($"Send Give persist Effect : id - {persistEffect.id}, time - {persistEffect.time} to {monsterID}");
            var message = CPacketFactory.CreateGivePersistEffect(monsterID, persistEffect);
            CTcpClient.instance.Send(message.data);
        }
        #endregion

        #region Portal, MapInfo
        public static void SendUsePortal()
        {
            var packet = CPacketFactory.CreatePortalPopup();

            CTcpClient.instance.Send(packet.data);
        }

        public static void SendPortalVote(int accept)
        {
            var packet = CPacketFactory.CreatePortalVote(accept);

            CTcpClient.instance.Send(packet.data);
        }

        public static void SendEnterNextStage()
        {
            var packet = CPacketFactory.CreateEnterNextStage();

            CTcpClient.instance.Send(packet.data);
        }

        public static void SendEnterNextRoom(int enteringRoomType, int enteringRoomNumber, int[] nextRoomTypeInfos)
        {
            var packet = CPacketFactory.CreateEnterNextRoom(enteringRoomType, enteringRoomNumber, nextRoomTypeInfos);

            CTcpClient.instance.Send(packet.data);
        }
        #endregion

        #region EventRoom

        public static void SendPopUpChooseNumber(int choose)
        {
            var packet = CPacketFactory.CreatePopUpChooseNumber(choose);

            CTcpClient.instance.Send(packet.data);
        }

        public static void SendEventRoom7SkillTable(bool[,] skillTable)
        {
            var packet = CPacketFactory.CreateEventRoom7SkillTable(skillTable);

            CTcpClient.instance.Send(packet.data);
        }

        public static void SendEventRoom7StartRace(bool start)
        {
            var packet = CPacketFactory.CreateEventRoom7StartRace(start);

            CTcpClient.instance.Send(packet.data);
        }

        #endregion


        #region Item
        private static void SendEarnItem(int itemCode)
        {
            Debug.Log("Send Earn Item");
            var message = CPacketFactory.CreateEarnItem(itemCode);
            CTcpClient.instance.Send(message.data);
        }
        #endregion

        private static void SendLodingFinish()
        {
            Debug.Log("Send Loading Finish Message");

            var packet = CPacketFactory.CreateFinishLoading(CClientInfo.JoinRoom.IsHost, CClientInfo.PlayerCount);

            CTcpClient.instance.Send(packet.data);
        }

        #endregion
    }
}
