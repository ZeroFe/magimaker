﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class CAudioManager : Singleton<CAudioManager>
{
    [Range(0,1)]
    public float AudioVolume = 0.5f;

    private void Start()
    {
        AudioListener.volume = AudioVolume;
    }
}
