﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAttachObjectEffect : CUseEffectHandle
{
    [SerializeField] private List<GameObject> attachedObjects = new List<GameObject>();

    public override void TakeUseEffect(CharacterPara cPara, GameObject giver = null)
    {
        foreach (var attachedObject in attachedObjects)
        {
            var attachedObjectInstance = Instantiate(attachedObject);
            cPara.AttachEffect(attachedObjectInstance);
        }
    }

    public override string Explain()
    {
        return "";
    }
}
