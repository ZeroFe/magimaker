﻿using UnityEngine;

public class CDefinedUseEffect : CUseEffectHandle
{
    /// <summary>
    /// Defined UseEfect에 맞춰 만든 Enum
    /// 자동으로 동기화되진 않으니 수동으로 해야함
    /// </summary>
    public enum EEffectName
    {
        Burn,
        Wet,
        Curse
    }

    private static readonly string[] EFFECT_NAMES = new string[] {
        "화상", "첨습", "저주",
    };

    [SerializeField]
    private EEffectName _effect;

    public override void TakeUseEffect(CharacterPara cPara, GameObject giver = null)
    {
        cPara.TakeUseEffect(GetUseEffectFromEnum(_effect), giver);
    }

    public override string Explain()
    {
        return EFFECT_NAMES[(int)_effect];
    }

    private CUseEffect GetUseEffectFromEnum(EEffectName effect)
    {
        return CDefinedEffectPool.Instance.GetDefinedUseEffectByIndex((int)effect);
    }
}
