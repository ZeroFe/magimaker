﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CMakeObjectUseEffect : CUseEffectHandle
{
    public GameObject shootObject;

    public override void TakeUseEffect(CharacterPara cPara, GameObject giver = null)
    {
        var shootObjectInstance = CSkillObjectPool.Instance.GetAvailableSkillObject(shootObject);
        if (giver)
        {
            Debug.Log("Make Object Effect");

            shootObjectInstance.SetActive(true);
            shootObjectInstance.tag = giver.tag;
            var hitObjectBase = shootObjectInstance.GetComponent<CHitObjectBase>();
            hitObjectBase.SetObjectUser(giver);
            hitObjectBase.SetObjectLayer(giver.layer);

            int layerMask = (1 << LayerMask.NameToLayer("Player"))
            | (1 << LayerMask.NameToLayer("PlayerSkill"))
            | (1 << LayerMask.NameToLayer("DeadBody"));
            layerMask = ~layerMask;
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
            {
                hitObjectBase.SetTransformInfo(giver.transform.position, hit.point);
            }
            else
            {
                hitObjectBase.SetTransformInfo(giver.transform.position, giver.transform.position + giver.transform.rotation.eulerAngles);
            }
            hitObjectBase.IsInit = true;
        }
        else
        {

        }
    }

    public override string Explain()
    {
        if (shootObject)
        {
            return $"{shootObject.name} 생성\n";
        }
        return "";
    }

    public override void EnhanceEffectByStat(CharacterPara cPara)
    {
        Debug.Log("MakeObjectUseEffect's EnhanceEffectByStat isn't updated");
    }
}
