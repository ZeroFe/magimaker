﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CInvincibleEffect : CUseEffectHandle
{
    [SerializeField] private float _invincibleTime = 3.0f;

    public override void TakeUseEffect(CharacterPara cPara, GameObject giver = null)
    {
        // 플레이어 캐릭터에만 적용되는 효과인지 고민 필요
        if (cPara is CPlayerPara)
        {
            var playerPara = cPara as CPlayerPara;

            playerPara._invincibilityChecker = true;
            playerPara.OnInvincibility();

            Debug.Log("UnknownReligionCross");

            StartCoroutine(WaitingOffInvincibility(playerPara, _invincibleTime));
        }
    }

    public override string Explain()
    {
        return $"{_invincibleTime}초 동안 무적\n";
    }

    IEnumerator WaitingOffInvincibility(CPlayerPara playerPara, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

        playerPara.OffInvincibility();
    }
}
