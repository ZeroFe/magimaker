﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CEarnGoldEffect : CUseEffectHandle
{
    [SerializeField] private int _earnGoldAmount = 5;

    public override void TakeUseEffect(CharacterPara cPara, GameObject giver = null)
    {
        if (cPara is CPlayerPara)
        {
            (cPara as CPlayerPara).Inventory.AddGold(_earnGoldAmount, false);
        }
    }

    public override string Explain()
    {
        return $"{_earnGoldAmount} 획득\n";
    }
}
