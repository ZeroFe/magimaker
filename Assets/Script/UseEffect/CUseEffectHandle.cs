﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CUseEffectHandleExplain
{
    public static string CreateUseEffectListText(List<CUseEffectHandle> effectList)
    {
        StringBuilder sb = new StringBuilder();
        foreach (var effect in effectList)
        {
            sb.Append(effect.Explain());
        }
        return sb.ToString();
    }
}

public abstract class CUseEffectHandle : MonoBehaviour
{
    public abstract void TakeUseEffect(CharacterPara cPara, GameObject giver = null);

    public abstract string Explain();

    /// <summary>
    /// 효과를 사용자 능력치에 따라 강화시킴
    /// </summary>
    /// <param name="userStatus"></param>
    public virtual void EnhanceEffectByStat(CharacterPara userStatus)
    {

    }
}
