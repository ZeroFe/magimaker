﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class CTestController : MonoBehaviour
{
    private Dictionary<KeyCode, Action> _selectedDictionary;
    private Dictionary<KeyCode, Action> _myCharacterTestDictionary;
    private Dictionary<KeyCode, Action> _characterControlDictionary;
    private Dictionary<KeyCode, Action> _monsterControlDictionary;
    private Dictionary<KeyCode, Action> _otherControlDictionary;

    [Header("Testing Components")]
    public CPlayerCommand commander;
    public CUIManager ui;
    public CMonsterManager monsterManager;

    [Header("Debug UI")]
    public TMPro.TMP_Text testedDictionaryText;
    [Header("Tested Character UI")]
    public GameObject characterTestPanel;

    public int SelectedCharacterNumber 
    {
        get { return _selectedCharacterNumber; }
        private set 
        {
            _selectedCharacterNumber = value;

            if (Network.CTcpClient.instance?.IsConnect == true && !CClientInfo.IsSinglePlay())
            {
                Debug.Log("Multiplay Test");
                var message = Network.CPacketFactory.CreateChangePlayer(_selectedCharacterNumber);

                Network.CTcpClient.instance.Send(message.data);
            }
        }
    }
    private int _selectedCharacterNumber = 1;

    // 보통 Dictionary 초기화는 Awake에서 하나,
    // Singleton instance의 타 함수들을 사용하는 해당 클래스는 안정성을 위해 Start에서 초기화 함
    private void Start()
    {
        var myCharacterPara = CController.instance.player.GetComponent<CharacterPara>();
        // 내 캐릭터 설정 모드
        _myCharacterTestDictionary = new Dictionary<KeyCode, Action>
        {
            // Character Damage / Heal
            [KeyCode.U] = () => myCharacterPara.DamagedDisregardDefence(300, null),
            [KeyCode.I] = () => myCharacterPara.Heal(300),
            // Characyer Death / Revive
            [KeyCode.J] = () => myCharacterPara.DamagedDisregardDefence(9999, null),
            [KeyCode.K] = () => myCharacterPara.Revive(50),
        };

        // 타 캐릭터 조종 모드
        _characterControlDictionary = new Dictionary<KeyCode, Action>
        {
            // Character Add / Delete
            [KeyCode.Insert] = () => commander.SetActivePlayers(4),
            [KeyCode.Delete] = () => KickPlayer(_selectedCharacterNumber),
            // Character Control
            [KeyCode.Alpha5]    = () => _selectedCharacterNumber = 0,
            [KeyCode.Alpha6]    = () => _selectedCharacterNumber = 1,
            [KeyCode.Alpha7]    = () => _selectedCharacterNumber = 2,
            [KeyCode.Alpha8]    = () => _selectedCharacterNumber = 3,
            [KeyCode.Alpha9]    = () => commander.SetMyCharacter(_selectedCharacterNumber),
            [KeyCode.U]         = () => commander.TakeDamage(_selectedCharacterNumber, 300),
            [KeyCode.I]         = () => commander.TakeHeal(_selectedCharacterNumber, 300),
            //[KeyCode.O]         = () => commander.TakePersistEffect(_selectedCharacterNumber, new CUseEffect.PersistEffect()),
            [KeyCode.J]         = () => commander.Follow(_selectedCharacterNumber),
            [KeyCode.K]         = () => commander.JumpMirror(_selectedCharacterNumber),
            [KeyCode.L]         = () => commander.RollMirror(_selectedCharacterNumber),
            [KeyCode.Semicolon] = () => commander.SkillTo(_selectedCharacterNumber),
            [KeyCode.M]         = () => CPortalManager.instance.SetPortalUseSelect(_selectedCharacterNumber, CPortalManager.EPortalVote.Accept),
            [KeyCode.Comma]     = () => CPortalManager.instance.SetPortalUseSelect(_selectedCharacterNumber, CPortalManager.EPortalVote.Cancel),
            // Item
            [KeyCode.KeypadPlus] = () => commander.EarnItem(_selectedCharacterNumber, 1),
        };

        _monsterControlDictionary = new Dictionary<KeyCode, Action>
        {
            // Set Order Mode
            [KeyCode.U] = () => monsterManager.SetOrderMode(true),
            [KeyCode.I] = () => monsterManager.SetOrderMode(false),
            // 
            [KeyCode.J] = () => monsterManager.HitAllMonster(),
            [KeyCode.K] = () => monsterManager.AttackAllMonster(),
            [KeyCode.L] = () => monsterManager.SkillAllMonster(1),
        };

        _otherControlDictionary = new Dictionary<KeyCode, Action>
        {
            [KeyCode.U] = () => CCreateMap.instance.PrintCurrentRoomInfo(),
            [KeyCode.J] = () => CCreateMap.instance.SetNextRandomExplicitRoom(CCreateMap.ERoomType.Normal),
            [KeyCode.K] = () => CCreateMap.instance.SetNextRandomExplicitRoom(CCreateMap.ERoomType.Event),
            [KeyCode.L] = () => CCreateMap.instance.SetNextRandomExplicitRoom(CCreateMap.ERoomType.Elite),
            [KeyCode.M] = () => CPortalManager.instance.ExecuteEnterNextRoom(0),
            [KeyCode.Comma] = () => CPortalManager.instance.ExecuteEnterNextRoom(1),
            [KeyCode.Period] = () => CPortalManager.instance.ExecuteEnterNextRoom(2),
            // 아이템
            [KeyCode.KeypadPlus] = () => CItemManager.SetItemToDropState(
                    CItemManager.instance.PopRandomItemByGrade(CItemManager.EItemGrade.Normal, CConstants.EQUIP_ITEM_TYPE),
                    GetHitPoint()),
            [KeyCode.KeypadMinus] = () => CItemManager.SetItemToDropState(
                    CItemManager.instance.PopRandomItemByGrade(CItemManager.EItemGrade.Normal, CConstants.CONSUM_ITEM_TYPE),
                    GetHitPoint()),
        };

        _selectedDictionary = _characterControlDictionary;
    }

    private void Update()
    {
        // 원격 조종 키를 바꿈
        if (Input.GetKeyDown(KeyCode.F5))
        {
            _selectedDictionary = _myCharacterTestDictionary;
            testedDictionaryText.text = "내 캐릭터 디버그 모드";
            characterTestPanel.SetActive(false);
            return;
        }
        if (Input.GetKeyDown(KeyCode.F6))
        {
            _selectedDictionary = _characterControlDictionary;
            testedDictionaryText.text = "캐릭터 조종 모드";
            characterTestPanel.SetActive(true);
            return;
        }
        else if (Input.GetKeyDown(KeyCode.F7))
        {
            _selectedDictionary = _monsterControlDictionary;
            testedDictionaryText.text = "몬스터 조종 모드";
            characterTestPanel.SetActive(false);
            return;
        }
        else if (Input.GetKeyDown(KeyCode.F8))
        {
            _selectedDictionary = _otherControlDictionary;
            testedDictionaryText.text = "그 외 설정 모드";
            characterTestPanel.SetActive(false);
            return;
        }

        if (Input.anyKeyDown)
        {
            foreach (var dic in _selectedDictionary)
            {
                if (Input.GetKeyDown(dic.Key))
                {
                    dic.Value();
                }
            }
        }

        if (monsterManager == null)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.Delete))
        {
            monsterManager.DestroyAllMonsters();
        }
    }

    private Vector3 GetHitPoint()
    {
        int layerMask = (1 << LayerMask.NameToLayer("Player")) | (1 << LayerMask.NameToLayer("PlayerSkill"));
        layerMask = ~layerMask;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
        {
            return hit.point;
        }

        return Vector3.one;
    }

    #region Debug Message Call
    private void KickPlayer(int playerNumber)
    {
        var message = Network.CPacketFactory.CreateKickPlayer(playerNumber);

        Network.CTcpClient.instance.Send(message.data);
    }

    private void RequestNextRoom()
    {
        var message = Network.CPacketFactory.CreateDebugRequsstRoomTypeInfo();

        Network.CTcpClient.instance.Send(message.data);
    }
    #endregion
}
