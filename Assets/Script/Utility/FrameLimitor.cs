﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameLimitor : MonoBehaviour
{
    public int FrameRate = 60;

    void Awake()
    {
        Application.targetFrameRate = FrameRate;
    }
}
