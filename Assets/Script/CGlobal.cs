﻿using UnityEngine;

public class CGlobal : MonoBehaviour
{
    public static int playerType;

    public static bool isClear = false; //CCreateMap에서 포탈 생성 조건
    public static bool isEvent = false; //갑작스레 몬스터가 생겨서 포탈 이동이 제한된다거나 할 때의 플래그
    public static bool popUpCancel = false; //팝업 끌지말지 결정용 플래그
    public static bool isEvRoom7Victory = true; //이벤트 룸7에서 사용하는 우승 플래그
}
