﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Network
{
    /*
     * 인게임에서의 패킷 변환을 담당하는 클래스
     * 네트워크에 연결된 경우 이벤트가 
     * 해석된 내용은 Commander로 실행시킨다
     */
    public static class CPacketInterpreter
    {
        private delegate void PacketInterpretImpl(CPacket packet);

        private static Dictionary<int, PacketInterpretImpl> _packetInterpretDict = new Dictionary<int, PacketInterpretImpl>()
        {
            // 캐릭터 움직임 관련
            [450] = InterpretSetCharacter,
            [451] = InterpretMoveStart,
            [452] = InterpretMoveCorrection,
            [453] = InterpretSkillCommand,
            [454] = InterpretJumpCommand,
            [455] = InterpretAttackCommand,
            [456] = InterpretRollCommand,
            // UseEffect 동기화
            [550] = TakeDamage,
            [551] = TakeHeal,
            [552] = TakePersistEffect,
            [553] = GiveDamage,
            [554] = GivePersistEffect,
            // 포탈, 맵
            [651] = InterpretPortalAccept,
            [652] = InterpretUsePortal,
            [653] = EnterNextStage,
            [654] = InterpretEnterNextRoom,
            [655] = InterpretNPCPopUpChoose,
            [656] = InterpretEventRoom7SkillTable,
            [657] = InterpretEventRoom7StartRace,
            // 아이템
            [851] = EarnItem,
            // 시스템
            [951] = InterpretReturnLobby,
            [952] = InterpretQuitGame,
            [953] = InterpretLoadingAllFinish,
        };

        public static void PacketInterpret(byte[] data)
        {
            // 헤더 읽기
            CPacket packet = new CPacket(data);
            packet.ReadHeader(out byte payloadSize, out short messageType);

            if (payloadSize != data.Length - CPacket.HEADER_SIZE)
            {
                Debug.Log($"Warning : packet read partial context. whole payloadSize = {data.Length - CPacket.HEADER_SIZE}");
            }

            if (_packetInterpretDict.TryGetValue((int)messageType, out var interpretFunc))
            {
                interpretFunc(packet);
            }
            else
            {
                Debug.Log($"Unknown Header : payloadSize = {payloadSize}, messageType = {messageType}");
            }
        }

        #region Interpret Packet
        #region Character Movement
        private static void InterpretSetCharacter(CPacket packet)
        {
            Int32 MyId = packet.ReadInt32();

            Debug.Log($"Set Character : my id - {MyId}");

            CPlayerCommand.instance.SetMyCharacter((int)MyId);
        }

        private static void InterpretMoveStart(CPacket packet)
        {
            Int32 id;
            Vector3 now, dest;

            id = packet.ReadInt32();
            now.x = packet.ReadSingle();
            now.y = packet.ReadSingle();
            now.z = packet.ReadSingle();
            dest.x = packet.ReadSingle();
            dest.y = packet.ReadSingle();
            dest.z = packet.ReadSingle();

            //Debug.LogFormat("Move Start - id{0} move ({1},{2},{3}) to ({4},{5},{6})", id, now.x, now.y, now.z, dest.x, dest.y, dest.z);

            CPlayerCommand.instance.Move(id, dest);
        }

        private static void InterpretMoveCorrection(CPacket packet)
        {
            Int32 id;
            Vector3 now;

            id = packet.ReadInt32();
            now.x = packet.ReadSingle();
            now.y = packet.ReadSingle();
            now.z = packet.ReadSingle();

            Debug.LogFormat("Move Stop - id{0} ({1},{2})", id, now.x, now.y, now.z);

            CPlayerCommand.instance.Teleport(id, now);
        }

        private static void InterpretSkillCommand(CPacket packet)
        {
            Int32 id;
            Int32 skillNumber;
            Vector3 now, dest;

            Debug.Log("action Command");

            id = packet.ReadInt32();
            skillNumber = packet.ReadInt32();
            now.x = packet.ReadSingle();
            now.y = packet.ReadSingle();
            now.z = packet.ReadSingle();
            dest.x = packet.ReadSingle();
            dest.y = packet.ReadSingle();
            dest.z = packet.ReadSingle();

            Debug.Log($"Use Skill - id{id} use skill number {skillNumber} in pos ({now.x},{now.y},{now.z}) to ({dest.x},{dest.y},{dest.z})");

            CPlayerCommand.instance.UseSkill((int)id, skillNumber, now, dest);
        }

        private static void InterpretJumpCommand(CPacket packet)
        {
            Debug.Log("Jump Command");

            Int32 id;
            Vector3 now;
            float rotateY;
            bool isMoving;

            id = packet.ReadInt32();
            now.x = packet.ReadSingle();
            now.y = packet.ReadSingle();
            now.z = packet.ReadSingle();
            rotateY = packet.ReadSingle();
            isMoving = packet.ReadBoolean();

            Debug.Log($"Jump Start - id{id} ({now.x},{now.y},{now.z}) with rotate ({rotateY})");

            CPlayerCommand.instance.Jump(id, now, rotateY);
        }

        private static void InterpretAttackCommand(CPacket packet)
        {
            Debug.Log("Attack Command");

            Int32 id;
            Vector3 now;
            float rotateY;

            id = packet.ReadInt32();
            now.x = packet.ReadSingle();
            now.y = packet.ReadSingle();
            now.z = packet.ReadSingle();
            rotateY = packet.ReadSingle();

            Debug.Log($"Attack Start - id{id} ({now.x},{now.y},{now.z}) with rotate ({rotateY})");

            CPlayerCommand.instance.Attack(id, now, rotateY);
        }

        private static void InterpretRollCommand(CPacket packet)
        {
            Debug.Log("Roll Command");

            Int32 id;
            Vector3 now;
            float rotateY;

            id = packet.ReadInt32();
            now.x = packet.ReadSingle();
            now.y = packet.ReadSingle();
            now.z = packet.ReadSingle();
            rotateY = packet.ReadSingle();

            Debug.Log($"Roll Start - id{id} ({now.x},{now.y},{now.z}) with rotate ({rotateY})");

            CPlayerCommand.instance.Roll(id, now, rotateY);
        }
        #endregion

        #region UseEfect
        private static void TakeDamage(CPacket packet)
        {
            int playerID = packet.ReadInt32();
            int damageAmount = packet.ReadInt32();

            Debug.Log($"Take Damage - Player{playerID} take {damageAmount}damage");

            CPlayerCommand.instance.TakeDamage(playerID, damageAmount);
        }

        private static void TakeHeal(CPacket packet)
        {
            int playerID = packet.ReadInt32();
            int healAmount = packet.ReadInt32();

            Debug.Log($"Take Heal - Player{playerID} take {healAmount}heal");

            CPlayerCommand.instance.TakeHeal(playerID, healAmount);
        }

        private static void TakePersistEffect(CPacket packet)
        {
            int playerID = packet.ReadInt32();
            CUseEffect.PersistEffect persistEffect = new CUseEffect.PersistEffect();
            persistEffect.id = packet.ReadInt32();
            persistEffect.time = packet.ReadSingle();
            persistEffect.increaseStack = packet.ReadInt32();
            persistEffect.maxStack = packet.ReadInt32();
            Debug.Log($"Take Persist Effect - Player{playerID + 1} take ID{persistEffect.id} effect for {persistEffect.time}sec " +
                $"{persistEffect.increaseStack}/{persistEffect.maxStack}");

            int changeAbilityCount = packet.ReadInt32();
            Debug.Log($"Change Ability Count = {changeAbilityCount}");
            for (int i = 0; i < changeAbilityCount; i++)
            {
                var ability = (EBuffAbility)packet.ReadInt32();
                var isBuff = packet.ReadInt32() == 1 ? true : false;
                var increaseBase = packet.ReadSingle();
                var increasePerStack = packet.ReadSingle();
                CUseEffect.ChangeAbilityInfo changeAbility = new CUseEffect.ChangeAbilityInfo(ability, isBuff, increaseBase, increasePerStack);
                persistEffect.changeAbilities.Add(changeAbility);
                Debug.Log(changeAbility.isBuff ? "Buff" : "Debuff" + $" {changeAbility.ability} {changeAbility.increaseBase}+{changeAbility.increasePerStack}%");
            }

            CPlayerCommand.instance.TakePersistEffect(playerID, persistEffect);
        }

        private static void GiveDamage(CPacket packet)
        {
            int monsterID = packet.ReadInt32();
            int damageAmount = packet.ReadInt32();

            Debug.Log($"Give Damage - Monster{monsterID} take {damageAmount}damage");

            CMonsterManager.instance.DamageTo(monsterID, damageAmount);
        }

        private static void GivePersistEffect(CPacket packet)
        {
            int monsterID = packet.ReadInt32();
            CUseEffect.PersistEffect persistEffect = new CUseEffect.PersistEffect();
            persistEffect.id = packet.ReadInt32();
            persistEffect.time = packet.ReadSingle();
            persistEffect.increaseStack = packet.ReadInt32();
            persistEffect.maxStack = packet.ReadInt32();
            Debug.Log($"Give Persist Effect - Monster{monsterID} take ID{persistEffect.id} effect for {persistEffect.time}sec " +
                $"{persistEffect.increaseStack}/{persistEffect.maxStack}");

            int changeAbilityCount = packet.ReadInt32();
            Debug.Log($"Change Ability Count = {changeAbilityCount}");
            for (int i = 0; i < changeAbilityCount; i++)
            {
                var ability = (EBuffAbility)packet.ReadInt32();
                var isBuff = packet.ReadInt32() == 1 ? true : false;
                var increaseBase = packet.ReadSingle();
                var increasePerStack = packet.ReadSingle();
                CUseEffect.ChangeAbilityInfo changeAbility = new CUseEffect.ChangeAbilityInfo(ability, isBuff, increaseBase, increasePerStack);
                persistEffect.changeAbilities.Add(changeAbility);
                Debug.Log(changeAbility.isBuff ? "Buff" : "Debuff" + $" {changeAbility.ability} {changeAbility.increaseBase}+{changeAbility.increasePerStack}%");
            }

            CMonsterManager.instance.PersistEffectTo(monsterID, persistEffect);
        }
        #endregion

        #region MapInfo
        private static void InterpretUsePortal(CPacket packet)
        {
            Int32 id;

            Debug.Log("Use Portal");

            id = packet.ReadInt32();

            CPortalManager.instance.SetActivePortalPopup(true);
            //playerCommander.UseSkill((int)id, (int)actionNumber, now, dest);
        }

        private static void InterpretPortalAccept(CPacket packet)
        {
            Int32 id;
            Int32 accept;

            Debug.Log("Portal Accept");

            id = packet.ReadInt32();
            accept = packet.ReadInt32();

            if (accept == 0)
            {
                CPortalManager.instance.SetPortalUseSelect(id, CPortalManager.EPortalVote.Accept);
            }
            else if (accept == 1)
            {
                CPortalManager.instance.SetPortalUseSelect(id, CPortalManager.EPortalVote.Cancel);
            }
        }

        private static void EnterNextStage(CPacket packet)
        {
            if (CCreateMap.instance.IsFinished)
            {
                // 피니시 처리
                Debug.Log("Finish!!!");
                //CWindowFacade.instance.OpenClearWindow();
            }
            else
            {
                CCreateMap.instance.EnterNextStage();

                var sendPacket = CPacketFactory.CreateFinishLoading(CClientInfo.JoinRoom.IsHost, CClientInfo.PlayerCount);

                CTcpClient.instance.Send(sendPacket.data);
            }
        }

        private static void InterpretEnterNextRoom(CPacket packet)
        {
            Debug.Log("Enter Next Room");

            int enteringRoomType = packet.ReadInt32();
            int enteringRoomNumber = packet.ReadInt32();
            int[] nextRoomTypeInfos = new int[3];
            nextRoomTypeInfos[0] = packet.ReadInt32();
            nextRoomTypeInfos[1] = packet.ReadInt32();
            nextRoomTypeInfos[2] = packet.ReadInt32();

            Debug.Log($"interpret : {enteringRoomType} {enteringRoomNumber} {nextRoomTypeInfos[0]},{nextRoomTypeInfos[1]},{nextRoomTypeInfos[2]}");

            CPortalManager.instance.MoveToNextRoom(enteringRoomType, enteringRoomNumber, nextRoomTypeInfos);
        }
        #endregion

        #region EventRoom

        private static void InterpretNPCPopUpChoose(CPacket packet)
        {
            Int32 choose = packet.ReadInt32();
            
            Debug.Log($"NPC PopUp Choose - {choose}");

            CNPCPopUpController.Instance.ReceiveChoosePlayerNumber(choose);
        }

        private static void InterpretEventRoom7SkillTable(CPacket packet)
        {
            bool[,] skillTable = new bool[4, 20];
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 20; j++)
                {
                    skillTable[i, j] = packet.ReadBoolean();
                }
            }

            Debug.Log("Interpret EventRoom7 SkillTable");

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 20; j++)
                {
                    Debug.Log("skillTable : " + skillTable[i, j]);
                }
            }

            CEventRoom7Manager.instance.ReceiveRunnerInfoToServer(skillTable);
        }

        private static void InterpretEventRoom7StartRace(CPacket packet)
        {
            bool start = packet.ReadBoolean();

            Debug.Log("Interpret EventRoom7 StartRace");

            (CRaceNpcPopUpController.Instance as CRaceNpcPopUpController).ReceiveStartRace(start);
        }

        #endregion

        #region Item
        private static void EarnItem(CPacket packet)
        {
            Debug.Log("Interpret Earn Item");
            int charID = packet.ReadInt32();
            int itemCode = packet.ReadInt32();
            CPlayerCommand.instance.EarnItem(charID, itemCode);
        }
        #endregion

        #region System
        private static void InterpretReturnLobby(CPacket packet)
        {
            CTcpClient.instance.DeletePacketInterpret();
            SceneManager.LoadScene("Lobby");
        }

        private static void InterpretQuitGame(CPacket packet)
        {
            CNetworkEvent.instance.QuitPlayer(packet.ReadInt32());
        }

        private static void InterpretLoadingAllFinish(CPacket packet)
        {
            Debug.Log("All player Loading finished");
            CWaitingLoadViewer.Instance.FinishLoading();
            if (CClientInfo.JoinRoom.IsHost)
            {
                CPortalManager.instance.ExecuteEnterNextRoom(0);
            }
        }
        #endregion
        #endregion

        #region Obsolete
        [Obsolete]
        private static void InterpretGetItem(CPacket packet)
        {
            Int32 id;

            Debug.Log("Get Item");

            id = packet.ReadInt32();

            GameObject.Destroy(GameObject.FindGameObjectWithTag("ITEM"));

            //playerCommander.UseSkill((int)id, (int)actionNumber, now, dest);
        }

        [Obsolete]
        private static void InterpretPortalTeleport(CPacket packet)
        {
            Int32 id;

            Debug.Log("Get Item");

            id = packet.ReadInt32();

        }

        [Obsolete]
        private static void InterpretRoomTypeInfos(CPacket packet)
        {
            Debug.Log("Receive Room Type Infomation Packets");

            int[,] rooms = new int[CConstants.ROOM_PER_STAGE, CConstants.MAX_ROAD];

            for (int i = 0; i < CConstants.ROOM_PER_STAGE; i++)
            {
                string roomData = $"rooms type row {i} : ";
                for (int j = 0; j < CConstants.MAX_ROAD; j++)
                {
                    rooms[i, j] = packet.ReadInt32();
                    roomData += rooms[i, j];
                }
                Debug.Log(roomData);
            }

            //CCreateMap.instance.ReceiveRoomArr(rooms);
        }

        [Obsolete]
        private static void InterpretRoomNumberInfos(CPacket packet)
        {
            Debug.Log("Receive Room Number Infomation Packets");

            int[,] rooms = new int[3, 10];

            for (int i = 0; i < 3; i++)
            {
                string roomData = $"rooms number row {i} : ";
                for (int j = 0; j < 10; j++)
                {
                    rooms[i, j] = packet.ReadInt32();
                    roomData += rooms[i, j];
                }
                Debug.Log(roomData);
            }

            // Stage Number 추가 필요
            //CCreateMap.instance.NonHostRoomEnqueue(rooms, 0);
        }
        #endregion
    }
}
