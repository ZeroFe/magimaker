﻿using System;

namespace Network
{
    /// <summary>
    /// 해당 큐는 dynamic RingBuffer라고 가정하며, empty일 때 외엔 front와 rear가 같아지는 일 없다고 볼 것임
    /// 실제 구현은 dynamic ringbuffer가 아니므로 오류가 생기면 수정할 것
    /// Concurrency를 위해 lock 사용. 
    /// </summary>
    internal class RingBuffer
    {
        public const int ringBufferDefaultSize = 10000;

        public bool IsEmpty { get { return _rear == _front; } }
        public int UsedSize { get { return (_rear - _front + Capacity) % Capacity; } }
        public int Capacity { get { return _buf.Length; } }
        public int FreeSize { get { return _buf.Length - UsedSize; } }

        // 한 번에 쓸 수 있는 길이
        // 원형 큐 구조상 버퍼 끝단에 있는 데이터는 끝 -> 처음으로 돌아감
        // 두 번에 끊어져서 쓰는 경우는 제외
        public int DirectEnqueueSize
        {
            get
            {
                return _front > _rear ? _front - _rear : Capacity - _rear;
            }
        }

        // 한 번에 읽을 수 있는 길이
        public int DirectDequeueSize
        {
            get
            {
                return _rear > _front ? _rear - _front : Capacity - _front;
            }
        }

        private int _front;
        private int _rear;
        private byte[] _buf;
        private readonly object _bufLock = new object();

        // 해당 큐에선 가득차는 일은 없다고 가정하겠음(Dynamic Queue로 가정)
        //private bool isFull { get { return (_rear + 1) % (_buf.Length + 1) == _front; } }

        public RingBuffer(int iBufferSize = ringBufferDefaultSize)
        {
            _front = 0;
            _rear = 0;
            _buf = new byte[iBufferSize];
        }

        public void Resize()
        {

        }

        public void Clear()
        {
            _front = 0;
            _rear = 0;
        }


        // 버퍼에 삽입
        public int Enqueue(byte[] chpData)
        {
            // 쓸 수 있는 양 확인
            int temp = FreeSize;
            int iSize = chpData.Length;
            if (temp < iSize)
            {
                iSize = temp;
            }

            lock (_bufLock)
            {
                // 버퍼에 쓰기
                temp = DirectEnqueueSize;
                // 한 번에 버퍼에 쓸 수 있는가?
                if (temp < iSize)
                {
                    // 두 번에 걸쳐서 버퍼에 쓰기
                    _Enqueue(chpData, 0, temp);
                    _Enqueue(chpData, temp, iSize - temp);
                }
                else
                {
                    _Enqueue(chpData, 0, iSize);
                }
            }

            return iSize;
        }

        // 패킷에 해당하는 버퍼 삽입
        // 패킷의 구조를 더 생각해보고 개선할 예정
        public int Enqueue(CPacket packet)
        {
            return Enqueue(packet.data);
        }

        // 버퍼에서 삭제
        public int Dequeue(byte[] chpData, int iSize)
        {
            int temp = UsedSize;

            if (temp < iSize)
            {
                iSize = temp;
            }

            if (iSize <= 0)
                return 0;

            lock (_bufLock)
            {
                temp = DirectDequeueSize;
                if (temp < iSize)
                {
                    _Dequeue(chpData, 0, temp);
                    _Dequeue(chpData, temp, iSize - temp);
                }
                else
                {
                    _Dequeue(chpData, 0, iSize);
                }
            }

            return iSize;
        }

        // 패킷의 구조를 더 생각해보고 개선할 예정
        public int Dequeue(CPacket packet, int iSize)
        {
            return Dequeue(packet.data, iSize);
        }

        // 게임 데이터 알아서 읽기
        public int Dequeue(out byte[] chpData)
        {
            // 페이로드 사이즈 위치 읽고 패킷 사이즈 구하기
            byte payloadSize = _buf[(_front + 1) % Capacity];
            int packetSize = payloadSize + 8;
            chpData = new byte[packetSize];

            return Dequeue(chpData, packetSize);
        }

        // 버퍼에 있는 내용을 data에 복제
        public int Peek(byte[] chpData, int iSize)
        {
            int temp = UsedSize;

            if (temp < iSize)
            {
                iSize = temp;
            }

            if (iSize <= 0)
                return 0;

            temp = DirectDequeueSize;
            if (temp < iSize)
            {
                Buffer.BlockCopy(_buf, _front, chpData, 0, temp);
                Buffer.BlockCopy(_buf, 0, chpData, temp, iSize - temp);
            }
            else
            {
                Buffer.BlockCopy(_buf, _front, chpData, 0, temp);
            }

            return iSize;
        }

        public bool CanDequeue()
        {
            // 패킷 최소 크기 충족하는가?
            if (UsedSize < 8)
            {
                return false;
            }

            byte payloadSize = _buf[(_front + 1) % Capacity];
            // 패킷 사이즈보다 작다면 Dequeue할 수 없음
            if (UsedSize >= payloadSize + 8)
            {
                return true;
            }

            return false;
        }

        // 버퍼에 쓰기(내부 함수)
        // 성능 문제 있으면 함수 없애고 변경할 것임
        private void _Enqueue(byte[] data, int dataOffset, int enqueueSize)
        {
            Buffer.BlockCopy(data, dataOffset, _buf, _rear, enqueueSize);
            _rear += enqueueSize;
            _rear %= Capacity;
        }

        // 버퍼에서 가져오기(내부 함수)
        // 성능 문제 있으면 함수 없애고 변경할 것임
        private void _Dequeue(byte[] data, int dataOffset, int dequeueSize)
        {
            Buffer.BlockCopy(_buf, _front, data, dataOffset, dequeueSize);
            _front += dequeueSize;
            _front %= Capacity;
        }
    }
}