﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SkillSelectEvent : UnityEvent<int> { }

public class SkillUseEvent : UnityEvent<int, Vector3> { }

/*
 * 모든 캐릭터의 스킬과 스킬 관련 정보를 저장하는 컴포넌트
 * 아이템 추가에 따라 데미지 증폭, 쿨다운 감소 등을 여기서 관리
 */

[RequireComponent(typeof(CSkillTimer))]
public class CCharacterSkill : MonoBehaviour
{
    protected static readonly int NOT_SELECTED = -1;

    [System.Serializable]
    protected class SkillFormat
    {
        [Tooltip("스킬의 이름")]
        public string skillName;
        [Tooltip("스킬 사용 시 나가는 오브젝트")]
        public GameObject skillObject;
        [Tooltip("스킬 쿨다운")]
        public float cooldown;
        [Tooltip("스킬 사용 시 취할 모션 번호")]
        public int actionNumber;
        [Tooltip("비축 가능한 스택")]
        public int maxStack;
        [Tooltip("스킬 썸네일")]
        public Sprite thumbnail;

        private int currentStack;

        public SkillFormat()
        {
            maxStack = 1;
            currentStack = 1;
        }

        public bool Use(Vector3 targetPos)
        {
            if (0 >= currentStack)
            {
                return false;
            }
            else
            {
                currentStack--;
                return true;
            }
        }

        public void EndCooldown(int notUsed)
        {
            currentStack++;
            if (currentStack > maxStack)
            {
                currentStack = maxStack;
            }
        }
    }

    [SerializeField]
    protected List<SkillFormat> _skillList = new List<SkillFormat>();

    public int SelectedSkillNum
    {
        get { return _selectedSkillNum; }
        protected set
        {
            _selectedSkillNum = value;
            skillSelectEvent?.Invoke(_selectedSkillNum);
        }
    }

    protected int _selectedSkillNum = 0;

    [System.NonSerialized]
    public GameObject currentUsingSkill = null;

    public SkillSelectEvent skillSelectEvent = new SkillSelectEvent();
    public SkillUseEvent skillUseEvent = new SkillUseEvent();

    protected virtual void Awake()
    {
    }

    /// <summary>
    /// 스킬 선택
    /// </summary>
    /// <param name="index"></param>
    public virtual void SkillSelect(int index)
    {
        if (index < 0 || index >= _skillList.Count)
        {
            Debug.Log("Skill Select Error");
            return;
        }

        SelectedSkillNum = index + 1;
    }

    public virtual void TrySkillToPosition(int skillNum, Vector3 targetPos)
    {
        if (skillNum == NOT_SELECTED)
        {
            Debug.Log("Skill Not Selected");
            return;
        }

        if (_skillList[skillNum].Use(targetPos))
        {
            GetComponent<CSkillTimer>().Register(skillNum, _skillList[skillNum].cooldown, _skillList[skillNum].EndCooldown);
            // CCntl의 행동 코드
            UseSkillToPosition(skillNum, targetPos);
        }
        SelectedSkillNum = 0;
    }

    public virtual void TrySkillToPosition(Vector3 targetPos)
    {
        TrySkillToPosition(SelectedSkillNum, targetPos);
    }

    public virtual void UseSkillToPosition(int skillNum, Vector3 targetPos)
    {
        skillUseEvent?.Invoke(skillNum, targetPos);
        currentUsingSkill = CreateSkillObject(_skillList[skillNum].skillObject, targetPos);
    }

    public Sprite GetSkillThumbnail(int skillNumber)
    {
        return _skillList[skillNumber].thumbnail;
    }

    // 스킬 오브젝트 생성
    protected GameObject CreateSkillObject(GameObject SkillPrefab, Vector3 targetPos)
    {
        if (SkillPrefab == null)
        {
            Debug.Log("Skill Object not setting");
            return null;
        }

        // 오브젝트 생성
        var skillObj = CSkillObjectPool.Instance.GetAvailableSkillObject(SkillPrefab);
        InitToSkillObject(skillObj, targetPos);
        return skillObj;
    }

    protected void InitToSkillObject(GameObject skillObj, Vector3 targetPos)
    {
        var hitObjectBase = skillObj.GetComponent<CHitObjectBase>();
        hitObjectBase.SetObjectUser(gameObject);
        hitObjectBase.SetTransformInfo(gameObject.transform.position, targetPos);

        // 값 설정 후 skillObj의 OnEnable 호출
        // 코드 순서 변경하면 버그 생길 수 있음
        skillObj.SetActive(true);
    }

    protected virtual void CallSkillUseEvent(int skillIndex, Vector3 targetPos)
    {
        skillUseEvent.Invoke(skillIndex, targetPos);
        CreateSkillObject(_skillList[_selectedSkillNum].skillObject, targetPos);
    }
}