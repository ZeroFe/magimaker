﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

[RequireComponent(typeof(CBuffTimer))]
public partial class CharacterPara : MonoBehaviour
{
    #region 총 능력치
    public virtual int TotalAttackMin
    {
        get { return (int)(_attackMin * _buffCoef[(int)EBuffAbility.Attack] * _debuffCoef[(int)EBuffAbility.Attack]); }
    }
    public virtual int TotalAttackMax
    {
        get { return (int)(_attackMax * _buffCoef[(int)EBuffAbility.Attack] * _debuffCoef[(int)EBuffAbility.Attack]); }
    }
    public virtual int TotalDefenece
    {
        get { return (int)(_defense * _buffCoef[(int)EBuffAbility.Defence] * _debuffCoef[(int)EBuffAbility.Defence]); }
    }
    public virtual int TotalMaxHp
    {
        get { return _maxHp; }
        protected set
        {
            var previousMaxHp = TotalMaxHp;
            _maxHp = value;
            Debug.Log($"Hp {previousMaxHp} => {value}");
            if(previousMaxHp < value)
            {
                CurrentHp = (int)(value * ((float)CurrentHp / previousMaxHp));
            }
            else if(value < CurrentHp)
            {
                CurrentHp = value;
            }
            HpDrawing?.Invoke(CurrentHp, TotalMaxHp);
        }
    }
    #endregion

    #region 캐릭터 기본 능력치
    public virtual int CurrentHp
    {
        get { return _curHp; }
        protected set 
        {
            if(value < 0)
            {
                _curHp = 0;
                if (!_isDead)
                {
                    Debug.Log("Dead?");
                    _isDead = true;
                    DeadEvent.Invoke();
                }
            }
            else if(value > TotalMaxHp)
            {
                _curHp = TotalMaxHp;
            }
            else
            {
                _curHp = value;
            }
            print(name + "'s HP: " + CurrentHp);
            HpDrawing?.Invoke(CurrentHp, TotalMaxHp);
        }
    }

    [Header("Status")]
    [Tooltip("최대 체력")] [SerializeField] public int _maxHp;
    public int _curHp;
    [Tooltip("최소 공격력")] [SerializeField] public int _attackMin;
    [Tooltip("최대 공격력")] [SerializeField] public int _attackMax;
    [Tooltip("방어력")] [SerializeField] public int _defense;
    public bool _isAnotherAction { get; set; }
    public bool _isStunned { get; set; }
    public bool _isDead { get; set; }
    [SerializeField] public int _rewardMoney;
    public int _spawnID { get; set; }
    [Tooltip("히트 애니메이션 출력\n최대체력 비율")] public float _hitGauge;
    #endregion

    #region 이벤트

    public class HitEvent : UnityEvent<int>
    {

    }

    /// <summary>
    /// 데미지를 주는 행위에 관한 이벤트
    /// int : 체력 변화량
    /// GameObject : 누구한테 주었는지
    /// </summary>
    public class GiveDamageEvent : UnityEvent<int, GameObject> { }
    public GiveDamageEvent GiveDamage { get; protected set; } = new GiveDamageEvent();
    public class GivePersistEvent : UnityEvent<CUseEffect.PersistEffect, GameObject> { }
    public GivePersistEvent GivePersistEffect { get; protected set; } = new GivePersistEvent();

    /// <summary>
    /// 지속 효과를 받을 때에 관한 이벤트
    /// </summary>
    public class TakePersistEffectEvent : UnityEvent<CUseEffect.PersistEffect> { }
    public TakePersistEffectEvent TakePersistEffect { get; protected set; } = new TakePersistEffectEvent();

    /// <summary>
    /// 체력이 바뀌는 모든 이벤트(데미지, 힐 등)
    /// int : 체력 변화량
    /// </summary>
    public class HpChanageEvent : UnityEvent<int> { }
    public HpChanageEvent TakeDamage { get; private set; } = new HpChanageEvent();
    public HpChanageEvent TakeHeal { get; private set; } = new HpChanageEvent();

    [System.NonSerialized]
    public UnityEvent hitEvent = new UnityEvent();

    public UnityEvent DeadEvent { get; private set; } = new UnityEvent();

    public HitEvent hitGaugeEvent = new HitEvent();

    // UI
    public class HpDrawingEvent : UnityEvent<int, int> { }
    public HpDrawingEvent HpDrawing { get; set; } = new HpDrawingEvent();
    #endregion

    protected CBuffTimer _buffTimer;

    public int RandomAttackDamage()
    {
        int _random = UnityEngine.Random.Range(_attackMin, _attackMax);
        return _random;
    }

    protected virtual void Awake()
    {
        _buffTimer = gameObject.GetComponent<CBuffTimer>();
        for (int i = 0; i < Enum.GetValues(typeof(EBuffAbility)).Length; i++)
        {
            _buffCoef[i] = 1.0f;
            _debuffCoef[i] = 1.0f;
        }

        InitCcVisualEffectPosition();
        DeadEvent.AddListener(RetriveCcVisualEffect);

        // 파라미터가 다른 이벤트 처리
        InitPara();
    }

    protected virtual void Start()
    {
        StartCoroutine("GetDamageOfTime");
    }

    protected void Update()
    {
        UpdateCcVisualEffectTime(Time.deltaTime);
    }

    public virtual void InitPara()
    {
        hitGaugeEvent.AddListener(HitGaugeCalculate);
    }

    // 평타 데미지 계산식
    public float GetRandomAttack()
    {
        float randAttack = UnityEngine.Random.Range(_attackMin, _attackMax + 1);
        // 최종 계산식 대충
        randAttack = randAttack - _defense;
        return randAttack;
    }

    public void SetEnemyAttack(float EnemyAttackPower)
    {
        // 데미지를 버림 형식으로 표현
        CurrentHp -= (int)EnemyAttackPower;
        //transform.gameObject.SendMessage("hitEnemyAttack");
    }

    // 방어력 계산식: 1000 / (950 + 10*방어력)
    public virtual void DamegedRegardDefence(int enemyAttack, GameObject giver)
    {
        int damage = enemyAttack * 1000 / (950 + 10 * TotalDefenece);
        DamagedDisregardDefence(damage, giver);
    }

    public virtual void DamagedDisregardDefence(int enemyAttack, GameObject giver)
    {
        CurrentHp -= (int)enemyAttack;
        TakeDamage?.Invoke(enemyAttack);
        hitGaugeEvent?.Invoke(enemyAttack);
        if (giver && giver.GetComponent<CharacterPara>())
        {
            giver.GetComponent<CharacterPara>().GiveDamage.Invoke(enemyAttack, giver);
        }
    }

    public virtual void HitGaugeCalculate(int attackDamage)
    {
        float result = (attackDamage * 100f) / _maxHp;
        if (result > _hitGauge)
        {
            Debug.Log("Hit Event Before");
            hitEvent.Invoke();
        }
    }

    public virtual void Heal(int amount)
    {
        CurrentHp += amount;
        TakeHeal?.Invoke(amount);
    }

    public virtual void Revive(float newCurrentHpPercent)
    {
        _isDead = false;
        CurrentHp = (int)((TotalMaxHp * newCurrentHpPercent) / 100);
    }
}