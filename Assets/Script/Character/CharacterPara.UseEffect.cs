﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public partial class CharacterPara : MonoBehaviour
{
    #region UseEffect 변수
    protected const float DoTCheckTime = 0.1f;
    public int useEffectAdditionalInfo = 0;

    protected class DamageOfTime
    {
        public static readonly float TimeBonus = 0.2f;

        public int id;
        public CUseEffect.HpChange Amount;
        public float CurrentTime;
        public float Period;

        public DamageOfTime(int id, CUseEffect.HpChange amount, float currentTime, float period)
        {
            this.id = id;
            Amount = amount;
            CurrentTime = currentTime;
            Period = period;
        }
    }

    [System.Serializable]
    protected class PersistStack
    {
        public int id;
        public int operatedStack;

        public PersistStack(int id, int operatedStack)
        {
            this.id = id;
            this.operatedStack = operatedStack;
        }
    }

    protected float[] _buffCoef = new float[Enum.GetValues(typeof(EBuffAbility)).Length];
    protected float[] _debuffCoef = new float[Enum.GetValues(typeof(EBuffAbility)).Length];
    protected List<DamageOfTime> _DoTList = new List<DamageOfTime>();
    protected List<PersistStack> _PersistStackList = new List<PersistStack>();
    #endregion

    #region 상태이상 Visual Effect 변수
    /// <summary>
    /// 기절 등 캐릭터 모션이 변화하는 경우가 아닌 첨습, 화상 등 이펙트가 달리는 경우
    /// 이외에도 몸에 부착시키는 효과 등에 사용 가능
    /// </summary>
    [Serializable]
    public class CcVisualEffectPosition
    {
        public Transform effectPoint;
        // 상황에 따라 머리 위 등 특수한 위치에 넣을 수 있도록 값 추가
    }

    [Header("CC Visual Effect")]
    [SerializeField] protected CcVisualEffectPosition _ccVisualEffectPosition = new CcVisualEffectPosition();

    // 부착된 Visual Effect 관리 리스트 (시간, Object)
    protected class VisualEffectInfo
    {
        public int ID;
        public float remainTime;
        public GameObject visualEffect;
    }

    protected List<VisualEffectInfo> _visualEffectInfos = new List<VisualEffectInfo>();
    #endregion

    #region UseEffect
    public void TakeUseEffectHandleList(List<CUseEffectHandle> effects, GameObject giver)
    {
        foreach (var effect in effects)
        {
            effect.TakeUseEffect(this, giver);
        }
    }

    //public void TakeUseEffectHandleList(List<CUseEffectHandle> effects, int count = 0)
    //{
    //    useEffectAdditionalInfo = count;

    //    foreach (var effect in effects)
    //    {
    //        effect.TakeUseEffect(this);
    //    }
    //}

    public virtual void TakeUseEffect(CUseEffect effect, GameObject giver)
    {
        if (effect == null || tag == "Allies")
        {
            return;
        }

        ApplyInstantEffect(effect.instantEffect, giver);
        ApplyConditionalEffect(effect.conditionalEffect, giver);
        ApplyPersistEffect(effect.persistEffect);
    }

    protected void ApplyInstantEffect(CUseEffect.InstantEffect instantEffect, GameObject giver)
    {
        // 데미지 효과 지정
        ApplyHpChange(instantEffect.hpChange, giver);
    }

    protected void ApplyConditionalEffect(CUseEffect.ConditionalEffect conditionalEffect, GameObject giver)
    {
        if (!conditionalEffect.IsValid())
        {
            return;
        }

        int effectStack = _buffTimer.GetBuffStack(conditionalEffect.conditionEffectId);
        if (effectStack >= 1)
        {
            // 스택 관련 옵션 지정 - 클래스라 값이 누적되는지 확인 필요
            if (conditionalEffect.isRelationStack)
            {
                conditionalEffect.effect.instantEffect.MultiplyPersant(effectStack * conditionalEffect.stackBonusRate);
            }

            TakeUseEffect(conditionalEffect.effect, giver);
        }
    }

    // 지속 효과 적용 방안
    // 버그 발생 확인 필요 : lambda로 캡쳐한 변수가 시간이 지나면 달라질 수 있음
    // 해결 방안 2 : Timer에는 id만 보내고 실제 내용은 CharacterPara에 LinkedList 만들어서 관리하기
    // public인 이유는 network를 통해 버프 / 디버프를 적용해야하기 때문
    public virtual void ApplyPersistEffect(CUseEffect.PersistEffect persistEffect)
    {
        // 초기화 값이 들어가는거 방지용
        if (persistEffect.IsValid())
        {
            AttachDefinedEffect(persistEffect.id, persistEffect.time);
            TakePersistEffect.Invoke(persistEffect);

            _buffTimer.Register(persistEffect.id, persistEffect.time, persistEffect.maxStack, persistEffect.increaseStack,
                (int buffStack) => StartPersistEffect(persistEffect, buffStack),
                (int buffStack) => EndPersistEffect(persistEffect, buffStack));
        }
    }

    // 지속 효과 시작
    protected void StartPersistEffect(CUseEffect.PersistEffect persistEffect, int stack)
    {
        if (persistEffect == null)
        {
            return;
        }

        ApplyTickHpChange(persistEffect.id, persistEffect.TickHpChange, persistEffect.TickPeriod);

        // CC 관련

        // 능력치 변화 관련
        foreach (var changeAbility in persistEffect.changeAbilities)
        {
            StartChangeAbility(changeAbility, stack);
        }

        // 스택 추가 효과 관련
        ApplyStackAccumalateEffect(persistEffect.id, persistEffect.stackAccumulateEffect);
    }

    // 지속 효과 끝
    protected void EndPersistEffect(CUseEffect.PersistEffect persistEffect, int stack)
    {
        // 지속 데미지(힐) 관련은 CCharacterPara에서 Update or Coroutine으로 관리
        // Coroutine 사용 이유 : 체력 변화는 오직 CCharacterPara 안에서만 일어나는 일
        // 체력 변화 대상 관리 등 복잡한 연산은 Timer에서 수행
        _DoTList.Remove(_DoTList.Find(x => x.id == persistEffect.id));

        // CC 관련

        // 능력치 변화 관련 
        foreach (var changeAbility in persistEffect.changeAbilities)
        {
            EndChangeAbility(changeAbility, stack);
        }

        // 스택 효과 관련
        if (_buffTimer.GetBuffStack(persistEffect.id) <= 0)
        {
            _PersistStackList.Remove(_PersistStackList.Find(x => x.id == persistEffect.id));
        }
    }

    protected void ApplyTickHpChange(int persistID, CUseEffect.HpChange tickHpChange, float tickPeriod)
    {
        if (tickHpChange == null || tickPeriod == 0)
        {
            return;
        }

        // 지속 데미지(힐) 관련은 CCharacterPara에서 Update or Coroutine으로 관리
        // Coroutine 사용 이유 : 체력 변화는 오직 CCharacterPara 안에서만 일어나는 일
        // 체력 변화 대상 관리 등 복잡한 연산은 Timer에서 수행
        if (tickHpChange.IsValid() && _DoTList.Find(x => x.id == persistID) == null)
        {
            _DoTList.Add(new DamageOfTime(persistID, tickHpChange, DamageOfTime.TimeBonus, tickPeriod));
        }
    }

    // 체력 변화 처리함수
    protected void ApplyHpChange(CUseEffect.HpChange hpChange, GameObject giver)
    {
        if (hpChange != null && !hpChange.IsValid())
        {
            return;
        }

        int totalAmount = hpChange.GetTotalAmount(CurrentHp, TotalMaxHp);
        if (hpChange.isHeal)
        {
            Heal(totalAmount) ;
        }
        else
        {
            if (hpChange.isTrueDamage)
            {
                DamagedDisregardDefence(totalAmount, giver);
            }
            else
            {
                DamegedRegardDefence(totalAmount, giver);
            }
        }
    }

    // 틱당 체력 변화 코루틴
    protected IEnumerator GetDamageOfTime()
    {
        while (true)
        {
            foreach (var DoT in _DoTList)
            {
                // 부동 소수점 계산으로 틱 계산이 불안정 - 틱을 손해볼 수 있음. 개선 필요
                if (DoT.CurrentTime >= DoT.Period)
                {
                    ApplyHpChange(DoT.Amount, null);
                    DoT.CurrentTime -= DoT.Period;
                }

                DoT.CurrentTime += DoTCheckTime;
            }
            yield return new WaitForSeconds(DoTCheckTime);
        }
    }

    protected void StartChangeAbility(CUseEffect.ChangeAbilityInfo changeAbility, int stack)
    {
        if (changeAbility.isBuff)
        {
            _buffCoef[(int)changeAbility.ability] += (changeAbility.increaseBase + changeAbility.increasePerStack * stack) * 0.01f;
        }
        else
        {
            _debuffCoef[(int)changeAbility.ability] *= 1.00f + (changeAbility.increaseBase + changeAbility.increasePerStack * stack) * 0.01f;
        }
    }

    protected void EndChangeAbility(CUseEffect.ChangeAbilityInfo changeAbility, int stack)
    {
        if (changeAbility.isBuff)
        {
            _buffCoef[(int)changeAbility.ability] -= (changeAbility.increaseBase + changeAbility.increasePerStack * stack) * 0.01f;
        }
        else
        {
            _debuffCoef[(int)changeAbility.ability] /= 1.00f + (changeAbility.increaseBase + changeAbility.increasePerStack * stack) * 0.01f;
        }
    }

    protected void ApplyStackAccumalateEffect(int id, CUseEffect.StackAccumulateEffect stackAccumEffect)
    {
        if (stackAccumEffect == null || stackAccumEffect.threshold == 0)
        {
            return;
        }

        int effectStack = _buffTimer.GetBuffStack(id);
        if (effectStack >= stackAccumEffect.threshold)
        {
            PersistStack persistStack;
            // 이미 효과가 발동됐는지 확인
            if ((persistStack = _PersistStackList.Find(x => x.id == id)) != null
                && persistStack.operatedStack >= effectStack)
            {
                return;
            }

            // 발동되지 않은 경우 다음 한계치 설정하기
            // 발동했을 때의 스택 계산 : effectStack > operateStack + n * limit인 operateStack + n * limit의 최대치
            // ex) 6에서 발동하는데 처음 발동한 스택이 12인 경우, 그리고 다음 발동 스택이 18인 경우 이전 발동 스택은 12로 계산되며 6은 발동하면 안 됨
            int previousOperatedStack = stackAccumEffect.threshold;
            while (previousOperatedStack + stackAccumEffect.thresholdAdd <= effectStack)
            {
                previousOperatedStack += stackAccumEffect.thresholdAdd;
            }
            if (persistStack == null)
            {
                _PersistStackList.Add(new PersistStack(id, previousOperatedStack));
            }
            else
            {
                persistStack.operatedStack = previousOperatedStack;
            }

            // 수정 필요
            TakeUseEffect(stackAccumEffect.effect, null);
        }
    }
    #endregion

    #region CC Visual Effect Implementation
    protected virtual void InitCcVisualEffectPosition()
    {
        if (!_ccVisualEffectPosition.effectPoint)
        {
            _ccVisualEffectPosition.effectPoint = transform;
        }
    }

    protected void UpdateCcVisualEffectTime(float decreaseTime)
    {
        for (int i = _visualEffectInfos.Count - 1; i >= 0; i--)
        {
            _visualEffectInfos[i].remainTime -= decreaseTime;
            if (_visualEffectInfos[i].remainTime <= 0)
            {
                DetachEffect(_visualEffectInfos[i].visualEffect);
                _visualEffectInfos.RemoveAt(i);
            }
        }
    }

    // 신체에 이펙트 효과 붙이기
    public void AttachEffect(GameObject effect)
    {
        if (!effect || !_ccVisualEffectPosition.effectPoint)
        {
            Debug.Log("Can't Attach Effect");
            return;
        }

        effect.transform.SetParent(_ccVisualEffectPosition.effectPoint);
        effect.transform.localPosition = Vector3.zero;
        effect.SetActive(true);
    }

    // 이펙트의 부모를 ObjectPool로 변경
    protected void DetachEffect(GameObject effect)
    {
        effect.transform.SetParent(CDefinedEffectPool.Instance.transform);
        effect.SetActive(false);
    }

    // 삭제 전에 이펙트 전부 반환
    protected virtual void RetriveCcVisualEffect()
    {
        foreach (var visualEffectInfo in _visualEffectInfos)
        {
            DetachEffect(visualEffectInfo.visualEffect);
        }
    }

    protected void AttachDefinedEffect(int id, float time)
    {
        // 지정된 이펙트가 있는지 확인하기 (id 이용)
        var isEffectAttached = _visualEffectInfos.Exists(x => x.ID == id);
        // 이펙트가 이미 붙어있다면 기존에 있던 이펙트 부착 시간을 재설정
        if (isEffectAttached)
        {
            _visualEffectInfos.Find(x => x.ID == id).remainTime = time;
        }
        // 이펙트가 안 붙어있다면 이펙트 풀로부터 이펙트 가져오고 이펙트 부착 시간 설정
        else
        {
            var particleEffect = CDefinedEffectPool.Instance.GetAvailableEffectParticle(id);
            if (!particleEffect)
            {
                Debug.Log("Can't Find Effect");
                return;
            }
            _visualEffectInfos.Add(new VisualEffectInfo(){
                ID = id,
                remainTime = time,
                visualEffect = particleEffect
            });
            AttachEffect(particleEffect);
        }
    }
    #endregion

    #region 추가적인 효과들
    /// <summary>
    /// Amount만큼 최대 체력을 증가시킨다(템에 의해서가 아님)
    /// </summary>
    /// <param name="amount"></param>
    public void DecreaseMaxHpAmount(int amount)
    {
        TotalMaxHp -= amount;
        Debug.Log($"Total Max HP = {TotalMaxHp}");
    }

    public void DecreaseMaxHpPercent(float percent)
    {
        int decreasedTotalMaxHP = (int)((1.0f - percent / 100.0f) * TotalMaxHp);
        TotalMaxHp = decreasedTotalMaxHP;
        Debug.Log($"Total Max HP = {TotalMaxHp}");
    }
    #endregion
}
