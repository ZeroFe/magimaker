﻿using UnityEngine;

public class CTutorialMapCreator : CCreateMap
{
    protected override void LoadSpecialRoom()
    {
        startRoom = Resources.Load("Room/StartRoomTuto0") as GameObject;
        bossRoom = Resources.Load("Room/BossRoomTuto0") as GameObject;
        shopRoom = Resources.Load("Room/0/ShopRoom0") as GameObject;
    }

    public override void CreateStage()
    {
        MakeNextRoomTypeInfos(_roomCount + 1);
    }

    protected override void MakeNextRoomTypeInfos(int roomCount)
    {
        Debug.Log("Tutorial Next Room Setting");

        if (_roomCount > 5) //방 전부 생성됬을경우 대기
            return;

        // 이전 방 내용 정리
        for (int i = 0; i < CConstants.MAX_ROAD; i++)
        {
            nextRoomTypeArr[i] = ERoomType.Empty;
        }

        if (_roomCount == 0)
        {
            nextRoomTypeArr[0] = ERoomType.Normal; //일반방 넣기
        }

        if (_roomCount == 1)
        {
            nextRoomTypeArr[0] = ERoomType.Event; //이벤트방 넣기
        }

        if (_roomCount == 2)
        {
            nextRoomTypeArr[0] = ERoomType.Elite; //엘리트방 넣기
        }

        if (_roomCount == 3)
        {
            nextRoomTypeArr[0] = ERoomType.Shop; //상점방 넣기
        }

        if (roomCount == CurrentStageMaxRoomCount - 1)
        {
            // 중앙에 보스 방 나오게 변경
            nextRoomTypeArr[1] = ERoomType.Boss;
            return;
        }

        MakePortalText();
    }
}