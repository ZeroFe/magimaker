﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using static System.Console;

public class CCreateMap : MonoBehaviour
{
    static public CCreateMap instance = null;

    [System.Serializable]
    protected class Stage
    {
        public int maxRoomCount = CConstants.ROOM_PER_STAGE;
        public List<GameObject> explicitRoomList = new List<GameObject>();
    }

    public enum ERoomType
    {
        Start,
        Normal,
        Event,
        Elite,
        Shop,
        Boss,
        Empty,
        Next,   // 다음 스테이지로 넘어가는 경우
    }

    #region room&portal
    protected GameObject startRoom;
    protected GameObject bossRoom;
    protected GameObject shopRoom;

    protected List<CPortal> _portals = new List<CPortal>();
    #endregion

    #region roomQueue
    public static readonly int CENTER_ROOM_POS = 1;
    public static readonly int ROOM_QUEUE_COUNTS = 3;
    public static readonly int VARIABLE_ROOMS_COUNT_IN_STAGE = CConstants.ROOM_PER_STAGE - 2;

    public Queue<GameObject> normalRoomQueue = new Queue<GameObject>();
    public Queue<GameObject> eventRoomQueue = new Queue<GameObject>();
    public Queue<GameObject> eliteRoomQueue = new Queue<GameObject>();
    //0노말 1이벤트 2엘리트
    protected Dictionary<ERoomType, Queue<GameObject>> _roomQueueDict = new Dictionary<ERoomType, Queue<GameObject>>();
    #endregion

    #region memberVar
    public int StageNumber { get; protected set; } = 0;
    public ERoomType UserSelectRoom { get; protected set; } = ERoomType.Empty;

    [SerializeField] protected List<Stage> _stages = new List<Stage>(1); // 스테이지는 무조건 하나 이상 있어야 함
    protected Stage CurrentStage { get { return _stages[StageNumber]; } }
    protected int CurrentStageMaxRoomCount { get { return CurrentStage.maxRoomCount; } }
    protected List<GameObject> CurrentExplicitRoomList { get { return CurrentStage.explicitRoomList; } }
    public bool IsStageLastRoom { get { return _roomCount == CurrentStageMaxRoomCount; } }
    public bool IsFinished { get { return StageNumber + 1 >= _stages.Count; } }

    protected int _eliteCount = 0;
    protected int _eventCount = 0;
    protected int _shopCount = 0;
    protected int _roomCount = 0;
    protected ERoomType[] nextRoomTypeArr = new ERoomType[CConstants.MAX_ROAD];
    protected GameObject _currentRoom;
    protected int _portalMomCount = 0; //포탈맘 사용할때 태그를 이용해서 오브젝트를 받아오는데, 이 때 사라져야할 전방 포탈들도 가져와서 전 방 포탈들을 따로 하드코딩으로 제외하기 위한 변수
    protected Dictionary<ERoomType, int> _roomCountDict = new Dictionary<ERoomType, int>();
    #endregion


    #region Init Function
    protected void Awake()
    {
        if (instance == null)
            instance = this;

        _currentRoom = GameObject.Find("TempRoom");

        // null 들어가는거 방지
        foreach (var stage in _stages)
        {
            if (stage.explicitRoomList == null)
            {
                stage.explicitRoomList = new List<GameObject>();
            }
        }

        _roomQueueDict.Add(ERoomType.Normal, normalRoomQueue);
        _roomQueueDict.Add(ERoomType.Event, eventRoomQueue);
        _roomQueueDict.Add(ERoomType.Elite, eliteRoomQueue);
        
        LoadStageRooms();
    }
    #endregion

    #region Portal 관련
    public void AddPortal()
    {
        GameObject[] portalMom = GameObject.FindGameObjectsWithTag("PORTAL_MOM");

        for (int i = _portalMomCount; i < portalMom.Length; i++)
            _portals.Add(portalMom[i].transform.Find("Portal").GetComponent<CPortal>());
    }

    public void RemovePortal()
    {
        _portals.Clear();
    }

    public void NotifyPortal()
    {
        foreach (CPortal portal in _portals)
        {
            if (portal != null)
                portal.OpenNClosePortal();
        }
    }

    public void MakePortalText(ERoomType leftRoomType, ERoomType middleRoomType, ERoomType rightRoomType)
    {
        GameObject[] portalMom = GameObject.FindGameObjectsWithTag("PORTAL_MOM");

        for (int i = _portalMomCount; i < portalMom.Length; i++)
        {
            Transform portal = portalMom[i].transform.Find("Portal");
            Transform text = portalMom[i].transform.Find("PortalText").Find("Text");

            switch (portal.tag)
            {
                case "LEFT_PORTAL":
                    text.GetComponent<TextMeshProUGUI>().text = leftRoomType.ToString();
                    portalMom[i].SetActive(leftRoomType == ERoomType.Empty ? false : true);
                    break;
                case "PORTAL":
                    text.GetComponent<TextMeshProUGUI>().text = middleRoomType.ToString();
                    portalMom[i].SetActive(middleRoomType == ERoomType.Empty ? false : true);
                    break;
                case "RIGHT_PORTAL":
                    text.GetComponent<TextMeshProUGUI>().text = rightRoomType.ToString();
                    portalMom[i].SetActive(rightRoomType == ERoomType.Empty ? false : true);
                    break;
            }
        }

        CGlobal.isClear = false; //포탈을 사용해서 새로운 방으로 왔으므로 방은 클리어되지 않은 상태
        NotifyPortal(); //플래그를 이용한 옵저버 패턴, 포탈 삭제하기
    }

    public void MakePortalText()
    {
        MakePortalText(nextRoomTypeArr[0], nextRoomTypeArr[1], nextRoomTypeArr[2]);
    }
    #endregion

    #region 방 생성 관련
    /// <summary>
    /// 내가 이동할 방의 정보[어느 방(방 타입 + 방 번호)으로 이동할지, 다음 방의 포탈 정보]를 만든다
    /// 스테이지 1(시작 지점)의 첫 방에서 세팅하는 것은 안 됨
    /// </summary>
    public System.Tuple<int, int, int[]> CreateNextRoomInfo(int roadNumber)
    {
        ERoomType selectedRoomType;
        int selectedRoomNumber;
        int[] nextRoomTypeIntArr = new int[3];

        if (IsExplicitRoom(_roomCount))
        {
            var explicitRoom = GetExplicitRoomInList(_roomCount);
            selectedRoomType = GetRoomType(explicitRoom);
            selectedRoomNumber = GetRoomNumber(explicitRoom);
        }
        else
        {
            selectedRoomType = nextRoomTypeArr[roadNumber];
            selectedRoomNumber = GetRoomNumber(GetNextRoom(selectedRoomType));
        }

        // 다음 방이 고정되어 있다면 가운데 포탈에 다음 방 표기
        if (IsExplicitRoom(_roomCount + 1))
        {
            // 이전 방 내용 정리
            for (int i = 0; i < nextRoomTypeIntArr.Length; i++)
            {
                nextRoomTypeIntArr[i] = (int)ERoomType.Empty;
            }
            var explicitRoom = GetExplicitRoomInList(_roomCount + 1);
            nextRoomTypeIntArr[CENTER_ROOM_POS] = (int)GetRoomType(explicitRoom);
        }
        else
        {
            MakeNextRoomTypeInfos(_roomCount + 1);
            for (int i = 0; i < nextRoomTypeIntArr.Length; i++)
            {
                nextRoomTypeIntArr[i] = (int)nextRoomTypeArr[i];
            }
        }

        return new System.Tuple<int, int, int[]>(
            (int)selectedRoomType,
            selectedRoomNumber,
            nextRoomTypeIntArr
            );
    }

    public virtual void CreateStage()
    {
        if (!CClientInfo.JoinRoom.IsHost)
        {
            Debug.Log("Guest can't create room type info");
            return;
        }

        if (_roomCount >= CurrentStageMaxRoomCount) //방 12개 전부 생성 시 대기
            return;

        MakeNextRoomTypeInfos(_roomCount);
    }

    protected virtual void MakeNextRoomTypeInfos(int roomCount)
    {
        // 이전 방 내용 정리
        for (int i = 0; i < CConstants.MAX_ROAD; i++)
        {
            nextRoomTypeArr[i] = ERoomType.Empty;
        }

        // 스테이지 막방이 보스 방이 되도록 강제 설정
        if (roomCount == CurrentStageMaxRoomCount - 1)
        {
            Debug.Log("Make Boss Room");
            // 중앙에 보스 방 나오게 변경
            nextRoomTypeArr[CENTER_ROOM_POS] = ERoomType.Boss;
            return;
        }

        // 다음 스테이지로 넘어가는 경우 설정
        if (roomCount == CurrentStageMaxRoomCount)
        {
            Debug.Log("Go Next Stage");
            nextRoomTypeArr[CENTER_ROOM_POS] = ERoomType.Next;
            return;
        }

        MakeRandomNextRooms(roomCount);
    }

    protected void MakeRandomNextRooms(int roomCount)
    {
        int randomRoad = Random.Range(0, CConstants.MAX_ROAD - 1); //랜덤한 갈림길 개수
        int selectRoomType = 123; //랜덤시드값
        int roadCount = 0; //갈림길 숫자
        for (int j = CConstants.MAX_ROAD; j > randomRoad; randomRoad++, roadCount++)
        {
            //최소 조건
            if (_shopCount < CConstants.MIN_SHOP_PER_STAGE && roomCount == (CurrentStageMaxRoomCount - 2) && roadCount == 0) //상점이 한번도 안나왔고 마지막 방일 때 첫번째 갈림길에는 무조건 상점방
            {
                nextRoomTypeArr[roadCount] = ERoomType.Shop;
                _shopCount++;
                continue;
            }

            selectRoomType = ((selectRoomType * Random.Range(0, 100)) + Random.Range(0, 50)) % 100;

            int probability = CConstants.ELITE_PROBABILITY;
            if (selectRoomType < probability
                && _eliteCount < 2 && roomCount > (((CConstants.ROOM_PER_STAGE - 2) / 2) - 1)) //엘리트                                                                                    
            {
                nextRoomTypeArr[roadCount] = ERoomType.Elite;
                _eliteCount++;
                continue;
            }

            probability += CConstants.EVENT_PROBABLILITY; //이벤트 방
            if (selectRoomType < probability && _eventCount < 15)
            {
                nextRoomTypeArr[roadCount] = ERoomType.Event;
                _eventCount++;
                continue;
            }

            probability += CConstants.NORMAL_PROBABILITY;
            if (selectRoomType < probability) //일반 방
            {
                nextRoomTypeArr[roadCount] = ERoomType.Normal;
                continue;
            }

            probability += CConstants.SHOP__PROBABILITY;
            if (selectRoomType < probability) // 상점
            {
                nextRoomTypeArr[roadCount] = ERoomType.Shop;
                _shopCount++;
                continue;
            }
        }
    }

    public void RoomFlagCtrl(ERoomType roomType)
    {
        if (roomType == ERoomType.Event ||
            roomType == ERoomType.Elite ||
            roomType == ERoomType.Normal ||
            roomType == ERoomType.Shop)
        {
            UserSelectRoom = roomType;
        }
    }

    /// <summary>
    /// 다음 방을 생성한다
    /// </summary>
    /// <param name="currentRoomType"></param>
    /// <param name="currentRoomNumber"></param>
    /// <param name="nextRoomTypeInfos"></param>
    public void InstantiateRoom(int currentRoomType, int currentRoomNumber, int[] nextRoomTypeInfos)
    {
        GameObject loadRoomOrigin = LoadRoom(StageNumber, (ERoomType)currentRoomType, currentRoomNumber);
        Debug.Log($"loaded room : {loadRoomOrigin.name}");
        var loadRoom = Instantiate(loadRoomOrigin);
        _currentRoom = loadRoom;

        AddPortal();
        MakePortalText((ERoomType)nextRoomTypeInfos[0], (ERoomType)nextRoomTypeInfos[1], (ERoomType)nextRoomTypeInfos[2]);
        _roomCount++;
    }

    public void DestroyRoom()
    {
        RemovePortal();
        Debug.Log("DestroyRoom : " + _currentRoom.name);
        Destroy(_currentRoom);

        //debug 용 태그로 방지우기
        //Object.Destroy(GameObject.FindGameObjectWithTag("DeleteRoom"));
        _currentRoom = null;
        return;
    }

    protected GameObject GetNextRoom(ERoomType roomType)
    {
        switch (roomType)
        {
            case ERoomType.Start: return startRoom;
            case ERoomType.Boss: return bossRoom;
            case ERoomType.Event: return eventRoomQueue.Dequeue();
            case ERoomType.Elite: return eliteRoomQueue.Dequeue();
            case ERoomType.Normal: return normalRoomQueue.Dequeue();
            case ERoomType.Shop: return shopRoom;
            case ERoomType.Empty: return null;
            default: return null;
        }
    }

    protected ERoomType GetRoomType(GameObject room)
    {
        var roomTypeString = room.name.Substring(0, System.Text.RegularExpressions.Regex.Match(room.name, "[0-9]").Index - 4);
        // ~Room이므로 ERoomType과 같은 이름을 찾아서 
        for (int roomType = 0; roomType < System.Enum.GetValues(typeof(ERoomType)).Length; roomType++)
        {
            if (((ERoomType)roomType).ToString() == roomTypeString)
            {
                return (ERoomType)roomType;
            }
        }
        return ERoomType.Empty;
    }

    protected int GetRoomNumber(GameObject room)
    {
        return int.Parse(room.name.Substring(System.Text.RegularExpressions.Regex.Match(room.name, "[0-9]").Index));
    }

    protected GameObject LoadRoom(int stageNumber, ERoomType currentRoomType, int roomNumber)
    {
        switch (currentRoomType)
        {
            case ERoomType.Start:
                return startRoom;
            case ERoomType.Normal:
                return Resources.Load($"Room/{stageNumber}/NormalRoom/NormalRoom{roomNumber}") as GameObject;
            case ERoomType.Event:
                return Resources.Load($"Room/{stageNumber}/EventRoom/EventRoom{roomNumber}") as GameObject;
            case ERoomType.Elite:
                return Resources.Load($"Room/{stageNumber}/EliteRoom/EliteRoom{roomNumber}") as GameObject;
            case ERoomType.Shop:
                return shopRoom;
            case ERoomType.Boss:
                return bossRoom;
            case ERoomType.Empty:
                Debug.Log("Can't load empty room");
                return null;
            default:
                return null;
        }
    }
    #endregion

    #region 스테이지 관련
    /// <summary>
    /// 싱글용 스테이지 넘어가는 임시 함수
    /// </summary>
    public void EnterNextStageSingle()
    {
        if (IsFinished)
        {
            CWindowFacade.instance.OpenClearWindow();
        }
        else
        {
            EnterNextStage();
            CPortalManager.instance.ExecuteEnterNextRoom(CENTER_ROOM_POS);
        }
    }

    /// <summary>
    /// 다음 스테이지로 넘어가면서 기존 스테이지 정보를 초기화하는 함수
    /// </summary>
    public void EnterNextStage()
    {
        StageNumber++;
        _eliteCount = 0;
        _eventCount = 0;
        _shopCount = 0;
        _roomCount = 0;

        for (int i = 0; i < nextRoomTypeArr.Length; i++)
        {
            nextRoomTypeArr[i] = ERoomType.Start;
        }

        LoadStageRooms();
    }

    protected void LoadStageRooms()
    {
        if (CClientInfo.JoinRoom.IsHost)
        {
            RandomRoomEnqueue(StageNumber);
        }
        LoadSpecialRoom();
        SetStageFirstRoom();
    }

    protected virtual void LoadSpecialRoom()
    {
        startRoom = Resources.Load($"Room/{StageNumber}/StartRoom0") as GameObject;
        bossRoom = Resources.Load($"Room/{StageNumber}/BossRoom0") as GameObject;
        shopRoom = Resources.Load($"Room/{StageNumber}/ShopRoom0") as GameObject;
    }

    protected void RandomRoomEnqueue(int stageNumber)  //스테이지의 종류에 따라 랜덤한 스테이지로 문자열을 만들어 반환함, random이 트루면 랜덤한 맵 리턴.
    {
        Debug.Log("Random Room Enqueue");

        string[] roomName = { $"Room/{stageNumber}/NormalRoom", $"Room/{stageNumber}/EventRoom", $"Room/{stageNumber}/EliteRoom" };

        // 큐에 넣기 전에 초기화
        _roomCountDict.Clear();
        foreach (var keyValuePair in _roomQueueDict)
        {
            keyValuePair.Value.Clear();
        }

        for (int i = 0; i < _roomQueueDict.Count; i++)
        {
            GameObject[] rooms = Resources.LoadAll<GameObject>(roomName[i]);
            _roomCountDict.Add((ERoomType)i + 1, rooms.Length);
            int count = 0;
            int rand;
            int j = 0;

            while (count < rooms.Length)
            {
                rand = Random.Range(0, rooms.Length);
                if (rooms[rand] != null)
                {
                    _roomQueueDict[(ERoomType)i + 1].Enqueue(rooms[rand]);
                    rooms[rand] = null;
                    count++;
                }
            }
        }
    }

    /// <summary>
    /// 스테이지의 초기 방을 설정한다 (방 고정한거 아니면 시작방, 보스방)
    /// </summary>
    protected void SetStageFirstRoom()
    {
        nextRoomTypeArr[CENTER_ROOM_POS] = CurrentStageMaxRoomCount == 1 ? ERoomType.Boss : ERoomType.Start;
    }
    #endregion

    #region Debug
    public void PrintCurrentRoomInfo()
    {
        Debug.Log($"currentRoom = {_currentRoom.name}");
        Debug.Log($"next Room = {nextRoomTypeArr[0]} {nextRoomTypeArr[1]} {nextRoomTypeArr[2]}");
    }

    public void SetNextRandomExplicitRoom(ERoomType roomType)
    {
        // 방 랜덤 픽
        int roomNumber;
        if (_roomCountDict.TryGetValue(roomType, out var count))
        {
            roomNumber = Random.Range(0, count);
        }
        else
        {
            roomNumber = 0;
        }

        for (int i = CurrentExplicitRoomList.Count; i <= _roomCount; i++)
        {
            CurrentExplicitRoomList.Add(null);
        }
        CurrentExplicitRoomList[_roomCount] = LoadRoom(StageNumber, roomType, roomNumber);
        Debug.Log($"{CurrentExplicitRoomList[_roomCount]}");
    }

    protected bool IsExplicitRoom(int roomCount)
    {
        return roomCount < CurrentExplicitRoomList.Count && CurrentExplicitRoomList[roomCount] != null;
    }

    protected GameObject GetExplicitRoomInList(int elementNumber)
    {
        return CurrentExplicitRoomList[elementNumber];
    }
    #endregion
}