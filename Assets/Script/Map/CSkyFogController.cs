﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class CSkyFogController : MonoBehaviour
{
    private VolumeProfile original;

    public Volume volume;
    public static CSkyFogController instance = null;
    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
            instance = this;
        volume = gameObject.GetComponent<Volume>();
        original = volume.profile;
    }

    public void ChangeVolume(VolumeProfile volumeProfile)
    {
        volume.profile = volumeProfile;
    }

    public void ChangeToOriginal()
    {
        volume.profile = original;
    }
}
