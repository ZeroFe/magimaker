﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CInGameQuitProcedure : DestroyableSingleton<CInGameQuitProcedure>
{
    public void QuitInGame()
    {
        // 현재 방을 삭제하여 작동 순서가 바뀌지 않게 한다
        CCreateMap.instance.DestroyRoom();

        // 서버 연결 상태에 따라 다른 씬으로 변경
        if (Network.CTcpClient.instance && Network.CTcpClient.instance.IsConnect)
        {
            if (CClientInfo.IsSinglePlay())
            {
                SceneManager.LoadScene("Lobby");
            }
            else
            {
                var message = Network.CPacketFactory.CreateReturnLobby(CClientInfo.JoinRoom.IsHost);

                Network.CTcpClient.instance.Send(message.data);
            }
        }
        else
        {
            SceneManager.LoadScene("Start");
        }
    }
}
